
(function (emr) {
    emr.on('storageLoaded', 'mapLoad');
    emr.fire('storageLoad');
    emr.on('storageLoaded2', 'mapLoad2');
    emr.fire('storageLoad2');
})(window.offlineMaps.eventManager);


function actualiza_menu_indicadores()
{
	$("#menu_indicadores").empty();

      db.transaction(function (tx) {
            tx.executeSql("SELECT * FROM indicadores ORDER BY indicadore_id ASC", [], function (tx, results) 
            {
                  if (results.rows && results.rows.length) 
                  {
		                  for (i= 0; i < results.rows.length; i = i + 1) {
		                        $("#menu_indicadores").append('<li id="mid'+results.rows.item(i).indicadore_id+'" onclick="cambia_indicador_menu('+results.rows.item(i).indicadore_id+')" class="leftmenu-opt"><a class="more" href="#"> <div style="display:inline-block;line-height:30px;margin-top:10px;margin-bottom:10px;"> <table border=0 cellpadding=0 cellspacing=0><tr><td width=25><i class="icon-th icon-white"></i></td><td>'+results.rows.item(i).nombre+'</td></tr></table></div></a></li>');
		                        if(results.rows.item(i).campo == '')
		                        	campos_indicadores_patentes[results.rows.item(i).indicadore_id] = 'giro_principal';
		                    	else
		                        	campos_indicadores_patentes[results.rows.item(i).indicadore_id] = results.rows.item(i).campo;
		                  }
                  }
                  else
                  {
		                        $("#menu_indicadores").append('<li class="leftmenu-opt"><a class="more" href="#"><i class="icon-th icon-white"></i>No hay indicadores<span id="badge_lm1" class="leftmenu-badge hide"></span></a></li>');
                  }
            }, function (tx) {
                console.log('Error en la sentencia: SELECT * FROM indicadores ORDER BY indicadore_id ASC');
            });
      });




}



function cambia_indicador_menu(indicadore_id)
{
		//Este codigo es solamente para deseleccionar el indicador seleccionado
		db.transaction(function (tx) {
		    tx.executeSql("SELECT * FROM indicadores", [], function (tx, results) 
		    {
		    	for (i= 0; i < results.rows.length; i = i + 1)
		    	{
		        	if(results.rows.item(i).indicadore_id == clave_indicador_actual)
		        	{
		              $("#txt_titulo_indicador").html(results.rows.item(i).nombre);
		              titulo_indicador_actual = results.rows.item(i).nombre;
		              $("#txt_descripcion_indicador").html("Descripci&oacute;n: " + results.rows.item(i).descripcion);
		              $('#mid'+results.rows.item(i).indicadore_id).addClass('leftmenu-activeopt');            		
		        	}
		        	else
		        	{
		              $('#mid'+results.rows.item(i).indicadore_id).removeClass('leftmenu-activeopt');            		
		        	}
		    	}

		    }, function (tx) {
		        console.log('Error en la sentencia: SELECT * FROM indicadores');
		    });
		});

		clave_indicador_actual = indicadore_id;
		muestra_datos_municipio();
		vista_datos_localidad();
		prepara_mapa_manzana();
		muestra_mapa_manzana();

}


function muestra_datos_localidad(id_grafico)
{
		if(id_grafico != -1)
			id_grafico_municipio_actual = id_grafico;
		
		vista_actual = 'localidad';
		clave_municipio_actual = lista_id_valor_graficos_municipio[id_grafico_municipio_actual];
		$("#txt_nombre_municipio").html("<span>"+lista_nombres_municipio_valor_grafico[id_grafico_municipio_actual]+"</span>");
		vista_datos_localidad();
		//controlador_vista_tablero(clave_indicador_actual);
}

function muestra_datos_manzana(id_grafico)
{
		id_grafico_localidad_actual = id_grafico;
		vista_actual = 'manzana';
		clave_localidad_actual = lista_id_valor_graficos_localidad[id_grafico_localidad_actual];
		prepara_mapa_manzana();
		//controlador_vista_tablero(clave_indicador_actual);
}



function muestra_datos_municipio()
{	

/*	mapa_localidad_referencia.eachLayer(function (layer) {
	    mapa_localidad_referencia.removeLayer(layer);
	});

	L.tileLayer('https://{s}.tiles.mapbox.com/v3/{id}/{z}/{x}/{y}.png', {
	    maxZoom: 18,
	    id: 'examples.map-i875mjb7',
	    detectRetina: true
	}).addTo(mapa_localidad_referencia);
*/

      //vista_actual = 'municipio';
      //id_grafico_municipio_actual = -1;
      //clave_municipio_actual = '';
//      $("#contenedor_vistas").load( "application/plantillas/vista_municipios.html", function() {

				//$("#tabla_municipios").empty();

				var datos_array = new Array();
				var datos_texto_array = new Array();
				var textos_color_array = new Array();
				lista_id_valor_graficos_municipio.length = 0;
				lista_nombres_municipio_valor_grafico.length = 0;

				db.transaction(function (tx) {
				    tx.executeSql("SELECT MIN(elemento) as elemento FROM datos_municipio_indicadores WHERE indicadore_id = ?", [clave_indicador_actual], function (tx, results) 
				    {

						if(results.rows.item(0).elemento == 'unico')
						{
					          //$("#encabezado_tabla_datos").empty();
					          //$("#encabezado_tabla_datos").append('<tr><th colspan="2">El Estado y sus municipios</th></tr>');
					          $("#div_tabla_datos").html('<table class="table table-conddensed margin-0"><tbody id="tabla_municipios" style="font-size: 12px;"></tbody></table>');
						      
						      db.transaction(function (tx) {
						            tx.executeSql("SELECT * FROM datos_municipio_indicadores WHERE indicadore_id = ? ORDER BY porcentaje DESC", [clave_indicador_actual], function (tx, results) 
						            {
									            	var i;
									            	for (i= 0; i < results.rows.length; i = i + 1)
									            	{

											            		if(results.rows.item(i).municipio == 'QUINTANA ROO')
											            			$('#tabla_municipios').append('<tr><td><div style="width:4px;height:0;border:5px solid rgba('+colores[i]+', 0.65);overflow:hidden"></div></td><td>'+results.rows.item(i).municipio+'</td><td>'+number_format( results.rows.item(i).valor, 0, '.', ',' )+'</td></tr><tr><td></td><td></td><td></td></tr>');
											            		else
											            			$('#tabla_municipios').append('<tr><td><div style="width:4px;height:0;border:5px solid rgba('+colores[i]+', 0.65);overflow:hidden"></div></td><td>'+results.rows.item(i).municipio+'</td><td>'+number_format( results.rows.item(i).valor, 0, '.', ',' )+'</td></tr>');

												            		$("#chart-top"+i+"_vframe").html(results.rows.item(i).municipio);
																	datos_array.length = 0;
																	textos_color_array.length = 0;
																	datos_array[0] = [0, parseFloat(results.rows.item(i).porcentaje.toFixed(2))];
																	textos_color_array[0] = ["", colores[i]]; 
												            		//chart_animpie("chart-top"+(i+1), 0, results.rows.item(i).porcentaje.toFixed(), true, colores[i], "190,190,190");
												            		
												            		chart_animpie2("chart-top"+i, JSON.stringify(datos_array) , JSON.stringify(textos_color_array), 0 );

												            		lista_id_valor_graficos_municipio[i] = results.rows.item(i).clave_municipio; 
												            		lista_nombres_municipio_valor_grafico[i] = results.rows.item(i).municipio; 

																	datos_array.length = 0;
																	textos_color_array.length = 0;
									            	}
									            	//$("#chart-top5_vframe").html("Resto de los municipios");
									            	

									            	//$("#chart-top5").width(400);
									            	//chart_animpie2("chart-top5", JSON.stringify(datos_array) , JSON.stringify(textos_color_array), 4 );
									            	$('#cuadro_simbologia').html('<span style="font-size:15px; top: 1px">Resto de <br>los municipios</span>');
						
						            }, function (tx) {
						                alert('Error en el select');
						            });
								});
				    	}
						else
						{
					          //$("#encabezado_tabla_datos").empty();
					          //$("#encabezado_tabla_datos").append('<tr><th colspan="3">El Estado y sus municipios</th></tr>');
					          $("#div_tabla_datos").html('<table class="table table-conddensed margin-0"><tbody id="tabla_municipios" style="font-size: 12px;"></tbody></table>');
						      db.transaction(function (tx) {
						            tx.executeSql("SELECT datos_municipio_indicadores.*, x.total FROM datos_municipio_indicadores JOIN "+
						            			  "(SELECT indicadore_id, clave_municipio, SUM(valor) as total FROM datos_municipio_indicadores GROUP BY indicadore_id, clave_municipio) as x "+
						            			  "ON (datos_municipio_indicadores.indicadore_id = x.indicadore_id AND datos_municipio_indicadores.clave_municipio = x.clave_municipio) "+
						            			  "WHERE datos_municipio_indicadores.indicadore_id = ? ORDER BY x.total DESC, datos_municipio_indicadores.clave_municipio ASC, datos_municipio_indicadores.elemento ASC", [clave_indicador_actual], function (tx, results) 
						            {
						            		if(results.rows.length > 0)
						            		{
									            	var i;
									            	var j = 0;
									            	var i_colores = 0;
									            	var municipio_actual = results.rows.item(0).municipio;
									            	var elemento = '';
									            	var codigo = '';
									            	var codigo_total = '';
									            	var codigo_tabla_simbologia = '<div style="font-size:18px;color:#545454;margin-bottom:5px;">Simbolog&iacute;a</div> <table><tbody>';
									            	lista_id_valor_graficos_municipio.push(results.rows.item(0).clave_municipio);
									            	lista_nombres_municipio_valor_grafico.push(results.rows.item(0).municipio);

									            	for (i= 0; i < results.rows.length; i = i + 1)
									            	{

														        if(results.rows.item(i).municipio == 'QUINTANA ROO')
														        {
														        	codigo_tabla_simbologia += '<tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px">'+
														        							   '<div style="width:8px;height:0;border:9px solid rgba('+colores[i_colores]+', 0.65);overflow:hidden"></div></div>'+
				                                                                               '</td><td class="legendLabel" style="font-size: 12px;">'+results.rows.item(i).elemento+'</td></tr>';
														        }

											            		//if(j < 5)
											            		//{
													            		if(municipio_actual != results.rows.item(i).municipio)
													            		{
														            		$("#chart-top"+j+"_vframe").html(municipio_actual);
														            		chart_animpie2("chart-top"+j, JSON.stringify(datos_array) , JSON.stringify(textos_color_array), 5 );
														            		codigo_total += '<tr><td valign="middle" rowspan="'+datos_array.length+'">'+codigo.substr(0, codigo.length - 4) ;
														            		codigo = '';

														            		codigo_total += '</tr><tr><td></td><td></td><td></td><td></td></tr>';

																			datos_array.length = 0;
																			textos_color_array.length = 0;
																			i_colores = 0;
																			lista_id_valor_graficos_municipio.push(results.rows.item(i).clave_municipio);
																			lista_nombres_municipio_valor_grafico.push(results.rows.item(i).municipio);

																			datos_array.push( [0, parseFloat(results.rows.item(i).porcentaje.toFixed(2))] );
																			textos_color_array.push( [results.rows.item(i).elemento, colores[i_colores]] );
																			municipio_actual = results.rows.item(i).municipio;
																			j++;
													            		}            			
													            		else
													            		{            		
																			datos_array.push( [0, parseFloat(results.rows.item(i).porcentaje.toFixed(2))] );
																			textos_color_array.push( [results.rows.item(i).elemento, colores[i_colores]] );
													            		}
											            		/*}
											            		else
											            		{

													            		if(municipio_actual != results.rows.item(i).municipio)
													            		{
														            		codigo_total += '<tr><td rowspan="'+datos_array.length+'">'+codigo.substr(0, codigo.length - 4) ;
														            		codigo = '';
														            		codigo_total += '</tr><tr><td></td><td></td><td></td></tr>';

																			datos_array.length = 0;
																			i_colores = 0;
																			datos_array.push( [0, parseFloat(results.rows.item(i).porcentaje.toFixed(2))] );
																			municipio_actual = results.rows.item(i).municipio;
													            		}            			
													            		else
													            		{            		
																			datos_array.push( [0, parseFloat(results.rows.item(i).porcentaje.toFixed(2))] );
													            		}

																	if(datos_texto_array[results.rows.item(i).elemento] == undefined)
																		datos_texto_array[results.rows.item(i).elemento] = parseFloat(results.rows.item(i).porcentaje.toFixed(2) );
																	else
																		datos_texto_array[results.rows.item(i).elemento] = parseFloat(datos_texto_array[results.rows.item(i).elemento]) + parseInt(results.rows.item(i).porcentaje.toFixed(2) );

																	j++;
																	//datos_texto_array.push([new Array('elemento',results.rows.item(i).elemento), new Array('valor', parseInt(results.rows.item(i).porcentaje.toFixed() )  ) ] );
											            		}*/

											            		if(elemento != results.rows.item(i).municipio)
											            		{
											            			codigo += results.rows.item(i).municipio+'</td>';
											            			elemento = results.rows.item(i).municipio;
											            		}
											            		
											            		codigo += '<td><div style="width:4px;height:0;border:5px solid rgba('+colores[i_colores]+', 0.65);overflow:hidden"></div></td>';
											            		codigo += '<td>'+results.rows.item(i).elemento+'</td>'+'<td>'+number_format( results.rows.item(i).valor, 0, '.', ',' )+'</td></tr>';
																codigo += '<tr>';

											            		i_colores++;

																if(i_colores > 10)
																	i_colores = 0;

									            	}

									            	codigo_total += '<tr><td rowspan="'+datos_array.length+'">'+codigo.substr(0, codigo.length - 4) ;
									            	$("#div_tabla_datos").html('<table class="table table-conddensed margin-0"><tbody id="tabla_municipios" style="font-size: 12px;">'+codigo_total+'</tbody></table>');
									            	//$('#tabla_municipios').html(codigo);

													/*datos_array.length = 0;
													textos_color_array.length = 0;
													i_colores = 0;
													var arr = [];

													for(property in datos_texto_array) {
														arr.push(property);
													    //console.log(property + " = " + datos_texto_array[property]);
													}

													for (var n=arr.length; n--; ){
													   var valor = datos_texto_array[arr[n]];

														datos_array.push( [0, parseFloat(valor.toFixed(2) )] );
														textos_color_array.push( [arr[n], colores[i_colores]] );
														i_colores++;
														if(i_colores > 10)
															i_colores = 0;
													}
													$("#chart-top5").width(250);
									            	chart_animpie2("chart-top5", JSON.stringify(datos_array) , JSON.stringify(textos_color_array), 5 );*/

								            		$("#chart-top10_vframe").html(municipio_actual);
								            		chart_animpie2("chart-top"+j, JSON.stringify(datos_array) , JSON.stringify(textos_color_array), 5 );

									            	$('#cuadro_simbologia').html(codigo_tabla_simbologia+'</tbody></table>');
									            	//$('#cuadro_simbologia').html('<table style="position:absolute;top:5px;right:5px;;font-size:smaller;color:#545454"><thead><tr><th colspan="2" style="font-size:18px;color:#545454">Simbolog&iacute;a</th></tr></thead><tbody><tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid rgba(0,0,255, 0.65);overflow:hidden"></div></div></td><td class="legendLabel">10%</td></tr><tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid rgba(0,147,0, 0.65);overflow:hidden"></div></div></td><td class="legendLabel">3%</td></tr></tbody></table>');
											}

						            }, function (tx) {
						                alert('Error en el select');
						            });
								});
				    	}


				    }, function (tx) {
				        alert('Error en el select');
				    });
				});

      //}); //callback al cargar la vista

}



function vista_datos_localidad()
{

	mapa_localidad_referencia.eachLayer(function (layer) {
	    mapa_localidad_referencia.removeLayer(layer);
	});

	mapa_localidad_referencia.addLayer(layer_street);

    var geojsonLayer = L.geoJson(limites_estatales_municipales, {        
        style: {
            "clickable": false,
            "color": "#AAAAAA",
            "weight": 5.0,
            "opacity": 0.9,
            "strokeDashstyle": "dash",
        }
    }).addTo(mapa_localidad_referencia);  

	$("#div_toploc1").hide();
	$("#div_toploc2").hide();
	$("#div_toploc3").hide();
	$("#div_toploc4").hide();

			var datos_array = new Array();
			var datos_texto_array = new Array();
			var textos_color_array = new Array();
			lista_id_valor_graficos_localidad.length = 0;

			db.transaction(function (tx) {
			    tx.executeSql("SELECT MIN(elemento) as elemento FROM datos_localidad_indicadores WHERE indicadore_id = ? AND clave_municipio = ?", [clave_indicador_actual, clave_municipio_actual], function (tx, results) 
			    {

					if(results.rows.item(0).elemento == 'unico')
					{
				          $("#div_tabla_datos_localidad").html('<table class="table table-conddensed margin-0"><tbody id="tabla_localidades" style="font-size: 12px;"></tbody></table>');
					      
					      db.transaction(function (tx) {
					            tx.executeSql("SELECT * FROM datos_localidad_indicadores WHERE indicadore_id = ? AND clave_municipio = ? ORDER BY porcentaje DESC", [clave_indicador_actual, clave_municipio_actual], function (tx, results) 
					            {
								            	var i, j=0, resto_localidades = 0.0;
								            	for (i= 0; i < results.rows.length; i = i + 1)
								            	{

										            		$('#tabla_localidades').append('<tr><td><div style="width:4px;height:0;border:5px solid rgba('+colores[i]+', 0.65);overflow:hidden"></div></td><td>'+results.rows.item(i).localidad+'</td><td>'+number_format( results.rows.item(i).valor, 0, '.', ',' )+'</td></tr>');

										            		if(i >= 5)
										            		{            		
																resto_localidades += parseFloat(results.rows.item(i).porcentaje);
										            		}
										            		else
										            		{
											            		$("#div_toploc"+j).show();
											            		$("#chart-toploc"+j+"_vframe").html(results.rows.item(i).localidad);
																datos_array.length = 0;
																textos_color_array.length = 0;
																datos_array[0] = [0, parseFloat(results.rows.item(i).porcentaje.toFixed(2))];
																textos_color_array[0] = ["", colores[i]]; 
											            		//chart_animpie("chart-top"+(i+1), 0, results.rows.item(i).porcentaje.toFixed(), true, colores[i], "190,190,190");
											            		chart_animpie2("chart-toploc"+j, JSON.stringify(datos_array) , JSON.stringify(textos_color_array), 0 );

											            		lista_id_valor_graficos_localidad[i] = results.rows.item(i).clave_localidad; 
											            		j++;
																datos_array.length = 0;
																textos_color_array.length = 0;
										            		}

								            	}

								            	if( j < results.rows.length)
								            	{
													datos_array.length = 0;
													textos_color_array.length = 0;
									            	datos_array.push( [0, parseFloat(resto_localidades.toFixed(2) )] );
													textos_color_array.push( ['', colores[j]] );
									            	//$("#chart-toploc5").width(400);
									            	$("#chart-toploc4_vframe").html('Resto de las localidades');
									            	chart_animpie2("chart-toploc4", JSON.stringify(datos_array) , JSON.stringify(textos_color_array), 0 );
								            	}
								            	$('#cuadro_simbologia_localidad').html('');								            		
					
					            }, function (tx) {
					                alert('Error en el select');
					            });
							});
			    	}
					else
					{
				          $("#div_tabla_datos_localidad").html('<table class="table table-conddensed margin-0"><tbody id="tabla_localidades" style="font-size: 12px;"></tbody></table>');
					      db.transaction(function (tx) {
					            tx.executeSql("SELECT datos_localidad_indicadores.*, x.total FROM datos_localidad_indicadores JOIN "+
					            			  "(SELECT indicadore_id, clave_municipio, clave_localidad, SUM(valor) as total FROM datos_localidad_indicadores GROUP BY indicadore_id, clave_municipio, clave_localidad) as x "+
					            			  "ON (datos_localidad_indicadores.indicadore_id = x.indicadore_id AND datos_localidad_indicadores.clave_municipio = x.clave_municipio AND datos_localidad_indicadores.clave_localidad = x.clave_localidad) "+
					            			  "WHERE datos_localidad_indicadores.indicadore_id = ? AND datos_localidad_indicadores.clave_municipio = ? ORDER BY x.total DESC, datos_localidad_indicadores.clave_localidad ASC, datos_localidad_indicadores.elemento ASC", [clave_indicador_actual, clave_municipio_actual], function (tx, results) 
					            {
					            		if(results.rows.length > 0)
					            		{
								            	var i;
								            	var j = 0;
								            	var i_colores = 0;
								            	var localidad_actual = results.rows.item(0).localidad;
								            	var elemento = '';
								            	var codigo = '';
								            	var codigo_total = '';
								            	var codigo_tabla_simbologia = '<div style="font-size:18px;color:#545454;margin-bottom:5px;">Simbolog&iacute;a</div> <table><tbody>';
								            	lista_id_valor_graficos_localidad.push(results.rows.item(0).clave_localidad);

								            	for (i= 0; i < results.rows.length; i = i + 1)
								            	{

										            		if(j < 5)
										            		{
												            		if(localidad_actual != results.rows.item(i).localidad)
												            		{
													            		$("#div_toploc"+j).show();
													            		$("#chart-toploc"+j+"_vframe").html(localidad_actual);
													            		chart_animpie2("chart-toploc"+j, JSON.stringify(datos_array) , JSON.stringify(textos_color_array), 5 );
													            		codigo_total += '<tr><td valign="middle" rowspan="'+datos_array.length+'">'+codigo.substr(0, codigo.length - 4) ;
													            		codigo = '';

													            		codigo_total += '</tr><tr><td></td><td></td><td></td><td></td></tr>';

																		datos_array.length = 0;
																		textos_color_array.length = 0;
																		i_colores = 0;
																		lista_id_valor_graficos_localidad.push(results.rows.item(i).clave_localidad);

																		datos_array.push( [0, parseFloat(results.rows.item(i).porcentaje.toFixed(2))] );
																		textos_color_array.push( [results.rows.item(i).elemento, colores[i_colores]] );
																		localidad_actual = results.rows.item(i).localidad;
																		j++;
												            		}            			
												            		else
												            		{            		
																		datos_array.push( [0, parseFloat(results.rows.item(i).porcentaje.toFixed(2))] );
																		textos_color_array.push( [results.rows.item(i).elemento, colores[i_colores]] );
												            		}
										            		}
										            		else
										            		{

												            		if(localidad_actual != results.rows.item(i).localidad)
												            		{
													            		codigo_total += '<tr><td rowspan="'+datos_array.length+'">'+codigo.substr(0, codigo.length - 4) ;
													            		codigo = '';
													            		codigo_total += '</tr><tr><td></td><td></td><td></td></tr>';

																		datos_array.length = 0;
																		i_colores = 0;
																		datos_array.push( [0, parseFloat(results.rows.item(i).porcentaje.toFixed(2))] );
																		localidad_actual = results.rows.item(i).localidad;
												            		}            			
												            		else
												            		{            		
																		datos_array.push( [0, parseFloat(results.rows.item(i).porcentaje.toFixed(2))] );
												            		}

																if(datos_texto_array[results.rows.item(i).elemento] == undefined)
																	datos_texto_array[results.rows.item(i).elemento] = parseFloat(results.rows.item(i).porcentaje.toFixed(2) );
																else
																	datos_texto_array[results.rows.item(i).elemento] = parseFloat(datos_texto_array[results.rows.item(i).elemento]) + parseFloat(results.rows.item(i).porcentaje.toFixed(2) );

																j++;
										            		}

													        if(j == 0)
													        {
													        	codigo_tabla_simbologia += '<tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px">'+
													        							   '<div style="width:8px;height:0;border:9px solid rgba('+colores[i_colores]+', 0.65);overflow:hidden"></div></div>'+
			                                                                               '</td><td class="legendLabel" style="font-size: 12px;">'+results.rows.item(i).elemento+'</td></tr>';

													        }

										            		if(elemento != results.rows.item(i).localidad)
										            		{
										            			codigo += results.rows.item(i).localidad+'</td>';
										            			elemento = results.rows.item(i).localidad;
										            		}
										            		
										            		codigo += '<td><div style="width:4px;height:0;border:5px solid rgba('+colores[i_colores]+', 0.65);overflow:hidden"></div></td>';
										            		codigo += '<td>'+results.rows.item(i).elemento+'</td>'+'<td>'+number_format( results.rows.item(i).valor, 0, '.', ',' )+'</td></tr>';
															codigo += '<tr>';

										            		i_colores++;

															if(i_colores > 10)
																i_colores = 0;

								            	}
												
								            	codigo_total += '<tr><td rowspan="'+datos_array.length+'">'+codigo.substr(0, codigo.length - 4) ;
								            	$("#div_tabla_datos_localidad").html('<table class="table table-conddensed margin-0"><tbody id="tabla_localidades" style="font-size: 12px;">'+codigo_total+'</tbody></table>');

												if( j >= 5)
												{
														$("#div_toploc4").show();
														$("#chart-toploc4_vframe").html('Resto de las localidades');
														datos_array.length = 0;
														textos_color_array.length = 0;
														i_colores = 0;
														var arr = [];

														for(property in datos_texto_array) {
															arr.push(property);
														    //console.log(property + " = " + datos_texto_array[property]);
														}

														for (var n=arr.length; n--; ){
														   var valor = datos_texto_array[arr[n]];

															datos_array.push( [0, parseFloat(valor.toFixed(2) )] );
															textos_color_array.push( [arr[n], colores[i_colores]] );
															i_colores++;
															if(i_colores > 10)
																i_colores = 0;
														}
								            			chart_animpie2("chart-toploc4", JSON.stringify(datos_array) , JSON.stringify(textos_color_array), 5 );
												}
												else
												{
													$("#div_toploc"+j).show();
													$("#chart-toploc"+j+"_vframe").html(localidad_actual);
								            		chart_animpie2("chart-toploc"+j, JSON.stringify(datos_array) , JSON.stringify(textos_color_array), 5 );
												}

								            	$('#cuadro_simbologia_localidad').html(codigo_tabla_simbologia+'</tbody></table>');
										}

					            }, function (tx) {
					                alert('Error en el select');
					            });
							});
			    	}


			    }, function (tx) {
			        alert('Error en el select');
			    });
			});


			//Funcion para mostrar el mapa a nivel localidad
			muestra_mapa_localidad(mapa_localidad_referencia, mapa_detalle=false, mostrar_simbologia=false, mostrar_texto_inicio=false);

}

