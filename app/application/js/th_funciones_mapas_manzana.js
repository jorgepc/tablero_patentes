function prepara_mapa_manzana()
{
    $("#resultado_busqueda_patentes").empty();
    $( "#tab_2_svalue" ).val('');
    $( "#select_busqueda_patentes_campo1" ).val('nombre_comercial_licencia');
    $( "#select_busqueda_patentes_municipio" ).val('000');
    
    $("#box_2_content").removeClass('in');
    $("#box_2_content").height(0);
    $('#box_2_expand').removeClass('icon-minus-sign').addClass('icon-plus-sign');
    $("#mapa_manzana").css('height', '685px' );

    //box-r-content collapse
    //box-r-content in collapse

    if(clave_municipio_actual != '')
    {
		    if (mapa_manzana_puntos.hasLayer(puntos_patentes))
		       mapa_manzana_puntos.removeLayer(puntos_patentes);

		    if (mapa_manzana_puntos.hasLayer(heatmapLayer))
		       mapa_manzana_puntos.removeLayer(heatmapLayer);
		    
		  	db.transaction(function (tx) {
		        tx.executeSql("SELECT xmin, ymin, xmax, ymax FROM layer_ciudades_extent WHERE clave_municipio = ? AND clave_localidad = ?", [clave_municipio_actual, clave_localidad_actual], function (tx, results) 
		        {
						if(results.rows.length > 0)
						    var extent = L.latLngBounds (L.latLng(results.rows.item(0).ymin, results.rows.item(0).xmin ), L.latLng(results.rows.item(0).ymax, results.rows.item(0).xmax));
						else
						    var extent = L.latLngBounds (L.latLng(municipios_extent_array[clave_municipio_actual+'_ymin'], municipios_extent_array[clave_municipio_actual+'_xmin'] ), L.latLng(municipios_extent_array[clave_municipio_actual+'_ymax'], municipios_extent_array[clave_municipio_actual+'_xmax']));
		 
		 				mapa_manzana_puntos.fitBounds(extent);

		        }, function (tx) {
		            alert('Error en el select');
		        });
			});

    }
}

function muestra_mapa_manzana()
{
	if(clave_municipio_actual != '')
	{
				tipo_markers = new Array();

			    //calculamos la simbologia tanto del mapa con de la legenda
				db.transaction(function (tx) {
				    tx.executeSql("SELECT MIN(elemento) as elemento FROM datos_localidad_indicadores WHERE indicadore_id = ? AND clave_municipio = ?", [clave_indicador_actual, clave_municipio_actual], function (tx, results) 
				    {
						if(results.rows.item(0).elemento == 'unico')
						{
					            campo_tabla_marker = '';
				            	html_cuadro_simbologia_manzana = '';
				            	ver_legenda_mapa_manzanas = false;
				            	tipo_markers['unico'] = colores_markers[0];
				            	pinta_capa_heatmap();
						}
						else
						{
						      db.transaction(function (tx) {
						            tx.executeSql("SELECT DISTINCT elemento FROM datos_localidad_indicadores WHERE indicadore_id = ? AND clave_municipio = ? ORDER BY elemento ASC", [clave_indicador_actual, clave_municipio_actual], function (tx, results) 
						            {
						            		if(results.rows.length > 0)
						            		{
									            	var i;
									            	var codigo_tabla_simbologia = '<div style="font-size:18px;color:#545454;margin-bottom:5px;">Simbolog&iacute;a</div> <table><tbody>';

									            	for (i= 0; i < results.rows.length; i = i + 1)
									            	{

														        	codigo_tabla_simbologia += '<tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px">'+
														        							   '<div style="width:8px;height:0;border:9px solid rgba('+colores[i]+', 0.65);overflow:hidden"></div></div>'+
				                                                                               '</td><td class="legendLabel" style="font-size: 12px;">'+results.rows.item(i).elemento+'</td></tr>';

				                                                    tipo_markers[results.rows.item(i).elemento] = colores_markers[i];

									            	}
													
									            	html_cuadro_simbologia_manzana = codigo_tabla_simbologia+'</tbody></table>';
									            	ver_legenda_mapa_manzanas = true;
									            	pinta_capa_heatmap();
											}

						            }, function (tx) {
						                alert('Error en el select');
						            });
								});
				    	}


				    }, function (tx) {
				        alert('Error en el select');
				    });
				});
	}
}


function pinta_capa_heatmap()
{
	var sql = "SELECT id, latitud, longitud FROM patentes WHERE clave_municipio = '"+clave_municipio_actual+"' AND clave_localidad = '"+clave_localidad_actual+"'";
		
  	db.transaction(function (tx) {
        tx.executeSql(sql, [], function (tx, results) 
        {
        				//console.log(results.rows.length);
		            	var i;
		            	var simbolo = '';

		            	datos_puntos_patentes_array = new Array();

		            	for (i= 0; i < results.rows.length; i = i + 1)
		            	{
					        datos_puntos_patentes_array.push('{"latitud": '+results.rows.item(i).latitud+', "longitud": '+results.rows.item(i).longitud+', "total": 1}'); 
					    }

 					    var data_heatmap = JSON.parse('{"max": 8, "data": ['+ datos_puntos_patentes_array.toString() + ']}');

					    heatmapLayer.setData(data_heatmap);

					    if(ver_legenda_mapa_manzanas == true)
					    {							    

							    if(legenda_mapa_manzanas_agregada == true)
							    	legend.removeFrom(mapa_manzana_puntos);

								legend.onAdd = function (map) {
								var div = L.DomUtil.create('div', 'info legend');
								    div.innerHTML = html_cuadro_simbologia_manzana;
									return div;
								};
								legend.addTo(mapa_manzana_puntos);
								legenda_mapa_manzanas_agregada = true;
					    }
					    else
					    {
							    if(legenda_mapa_manzanas_agregada == true)
							    	legend.removeFrom(mapa_manzana_puntos);
						    	
						    	legenda_mapa_manzanas_agregada = false;
					    }

        }, function (tx) {
            alert('Error en el select de los puntos');
        });
	});
}


function pinta_capa_patentes()
{
	var lng_min = mapa_manzana_puntos.getBounds().getSouthWest().lng;
	var lat_min = mapa_manzana_puntos.getBounds().getSouthWest().lat;
	var lng_max = mapa_manzana_puntos.getBounds().getNorthEast().lng;
	var lat_max = mapa_manzana_puntos.getBounds().getNorthEast().lat;

	var sql = "SELECT id, nombre_comercial_licencia, tipo_establecimiento, giro, giro_principal, domicilio_establecimiento, municipio, localidad, latitud, longitud, "+campos_indicadores_patentes[clave_indicador_actual]+" as campo_indicador_actual "+
			  "FROM patentes WHERE (longitud >= "+lng_min+") AND (longitud <= "+lng_max+") AND (latitud >= "+lat_min+" ) AND (latitud <= "+lat_max+")";
	
	//Esto es para mostrar datos del mapa
  	db.transaction(function (tx) {
        tx.executeSql(sql, [], function (tx, results) 
        {
        				console.log(results.rows.length);
		            	var i;
		            	var simbolo = '';

		            	puntos_patentes.clearLayers();

		            	for (i= 0; i < results.rows.length; i = i + 1)
		            	{
					        if(results.rows.item(i).giro_principal == 'ALIMENTOS')
					        	simbolo = 'cutlery';
					        else if(results.rows.item(i).giro_principal == 'COPEO')
					        	simbolo = 'glass';
					        else if(results.rows.item(i).giro_principal == 'DISTRIBUCIÓN')
					        	simbolo = 'users';
					        else if(results.rows.item(i).giro_principal == 'ENVASE CERRADO')
					        	simbolo = 'beer';
					        else
					        	simbolo = 'cog';

					        if(tipo_markers['unico'] === undefined)
					        	var marker = L.marker([results.rows.item(i).latitud, results.rows.item(i).longitud], {id: results.rows.item(i).id, icon: L.AwesomeMarkers.icon({icon: simbolo, prefix: 'fa', markerColor: tipo_markers[results.rows.item(i).campo_indicador_actual], iconColor: 'white' }) });
							else					        
					        	var marker = L.marker([results.rows.item(i).latitud, results.rows.item(i).longitud], {id: results.rows.item(i).id, icon: L.AwesomeMarkers.icon({icon: simbolo, prefix: 'fa', markerColor: tipo_markers['unico'], iconColor: 'white' }) });
					        
					        marker.bindPopup("<strong>Nombre:</strong> "+results.rows.item(i).nombre_comercial_licencia+"<br><strong>Tipo:</strong> "+results.rows.item(i).tipo_establecimiento+"<br><strong>Giro:</strong> "+results.rows.item(i).giro+"<br><strong>Domicilio:</strong> "+results.rows.item(i).domicilio_establecimiento+"<br><strong>Localidad:</strong> "+results.rows.item(i).localidad+", "+results.rows.item(i).municipio, {
					            showOnMouseOver: false
					        });

					        puntos_patentes.addLayer(marker);
					    }

        }, function (tx) {
            alert('Error en el select de los puntos');
        });
	});
}



function cambia_mapa_fondo()
{
    checarconexion(function (conectividad) {
                   if(conectividad == 1)
                   {
                        if (mapa_manzana_puntos.hasLayer(layer_satelite))
                        {
                            mapa_manzana_puntos.removeLayer(layer_satelite);
                            layer_satelite_encendido = false;
                        }
                        else
                        {
                            mapa_manzana_puntos.addLayer(layer_satelite);
                            layer_satelite_encendido = true;
                        }
                   }
                   else
                   {
                        alert("El dispositivo no tiene conexi\u00f3n a Internet, por lo tanto no se puede mostrar la imagen de sat\u00e9lite");
                   }
                   
    });
}


function buscador_patentes()
{
	var cvemun_buscar = $( "#select_busqueda_patentes_municipio" ).val();
	var campo_buscar = $( "#select_busqueda_patentes_campo1" ).val();
	var valor_buscar = $( "#tab_2_svalue" ).val();

	if(cvemun_buscar == '000')
	{
		alert("Seleccione primero el municipio en donde se va a realizar la b\u00FAsqueda");
		return true;
	}
	$("#resultado_busqueda_patentes").empty();

		  	db.transaction(function (tx) {
		        tx.executeSql("SELECT id, nombre_comercial_licencia, numero_licencia, tipo_establecimiento, giro, domicilio_establecimiento, clave_municipio, municipio, clave_localidad, localidad, monto_adeudo, comodato, costo_patente, longitud, latitud FROM patentes WHERE clave_municipio = '"+cvemun_buscar+"' AND "+campo_buscar+" LIKE '%"+valor_buscar+"%' LIMIT 20", [], function (tx, results) 
		        {
						if(results.rows.length > 0)
						{
							if(results.rows.length == 20)
							{
								alert("La consulta arroj\u00F3 muchos resultados, por lo tanto solo se mostrar\u00E1n los primeros 20, por favor especifique m\u00E1s su b\u00FAsqueda");
							}

							var codigo = '';
							for (var i= 0; i < results.rows.length; i = i + 1)
							{
								codigo = '<tr><td><button class="btn btn-mini pull-right" onclick="muestra_patente_mapa('+results.rows.item(i).id+',\''+results.rows.item(i).longitud+'\',\''+results.rows.item(i).latitud+'\')" type="button"><i class="icon-map-marker"></i></button></td><td>'+(i+1)+'</td>';
								codigo += '<td>'+results.rows.item(i).nombre_comercial_licencia+'</td>';
								codigo += '<td>'+results.rows.item(i).numero_licencia+'</td>';
								codigo += '<td>'+results.rows.item(i).tipo_establecimiento+'</td>';
								codigo += '<td>'+results.rows.item(i).giro+'</td>';
								codigo += '<td>'+results.rows.item(i).domicilio_establecimiento+'</td>';
								codigo += '<td>'+results.rows.item(i).municipio+'</td>';
								codigo += '<td>'+results.rows.item(i).localidad+'</td>';
								codigo += '<td>'+results.rows.item(i).monto_adeudo+'</td>';
								codigo += '<td>'+results.rows.item(i).comodato+'</td>';
								codigo += '<td>'+results.rows.item(i).costo_patente+'</td></tr>';
								$('#resultado_busqueda_patentes').append(codigo);
							}
						}
						else
						{
							alert("No se encontr\u00F3 ning\u00FAn elemento que coincida con la b\u00FAsqueda");
						}

		        }, function (tx) {
		            alert('Error en la busqueda de patentes');
		        });
			});
}


function muestra_patente_mapa(id, longitud, latitud)
{
	
		mapa_manzana_puntos.setView(L.latLng(latitud, longitud), 17)
	
		setTimeout(function()
		{
				var encontrado = false;
				puntos_patentes.eachLayer(function(feature) {  				
					if(id == feature.options.id)
					{
						feature.openPopup();
						encontrado = true;
						return false;				
					}
				});
		}, 1000);
		/*
		if(encontrado == false)
		{
				agrega_feature_nuevo(id, function(marker) {
						mapa_manzana_puntos.setView(marker.getLatLng(), 17)
						setTimeout(function(){marker.openPopup();}, 1000);					
				});
		}*/

}


function agrega_feature_nuevo(id, callback)
{
	var sql = "SELECT id, nombre_comercial_licencia, tipo_establecimiento, giro, giro_principal, domicilio_establecimiento, municipio, localidad, latitud, longitud, "+campos_indicadores_patentes[clave_indicador_actual]+" as campo_indicador_actual FROM patentes WHERE id = "+id;
	
	//Esto es para mostrar datos del mapa
  	db.transaction(function (tx) {
        tx.executeSql(sql, [], function (tx, results) 
        {
        		if( results.rows.length > 0)
        		{
		            	var i;
		            	var simbolo = '';

				        datos_puntos_patentes_array.push('{"latitud": '+results.rows.item(0).latitud+', "longitud": '+results.rows.item(0).longitud+', "total": 1}'); 

				        if(results.rows.item(0).giro_principal == 'ALIMENTOS')
				        	simbolo = 'cutlery';
				        else if(results.rows.item(0).giro_principal == 'COPEO')
				        	simbolo = 'glass';
				        else if(results.rows.item(0).giro_principal == 'DISTRIBUCIÓN')
				        	simbolo = 'users';
				        else if(results.rows.item(0).giro_principal == 'ENVASE CERRADO')
				        	simbolo = 'beer';
				        else
				        	simbolo = 'cog';

				        if(tipo_markers['unico'] === undefined)
				        	var marker = L.marker([results.rows.item(0).latitud, results.rows.item(0).longitud], {id: results.rows.item(0).id, icon: L.AwesomeMarkers.icon({icon: simbolo, prefix: 'fa', markerColor: tipo_markers[results.rows.item(0).campo_indicador_actual], iconColor: 'white' }) });
						else					        
				        	var marker = L.marker([results.rows.item(0).latitud, results.rows.item(0).longitud], {id: results.rows.item(0).id, icon: L.AwesomeMarkers.icon({icon: simbolo, prefix: 'fa', markerColor: tipo_markers['unico'], iconColor: 'white' }) });
				        
				        marker.bindPopup("<strong>Nombre:</strong> "+results.rows.item(0).nombre_comercial_licencia+"<br><strong>Tipo:</strong> "+results.rows.item(0).tipo_establecimiento+"<br><strong>Giro:</strong> "+results.rows.item(0).giro+"<br><strong>Domicilio:</strong> "+results.rows.item(0).domicilio_establecimiento+"<br><strong>Localidad:</strong> "+results.rows.item(0).localidad+", "+results.rows.item(0).municipio, {
				            showOnMouseOver: false
				        });

				        puntos_patentes.addLayer(marker);

 					    var data_heatmap = JSON.parse('{"max": 8, "data": ['+ datos_puntos_patentes_array.toString() + ']}');

					    heatmapLayer.setData(data_heatmap);

						callback(marker);		    

        		}

        }, function (tx) {
            alert('Error en el select');
            callback(undefined);
        });
	});
}



function vista_inicial()
{
		  	db.transaction(function (tx) {
		        tx.executeSql("SELECT xmin, ymin, xmax, ymax FROM layer_ciudades_extent WHERE clave_municipio = ? AND clave_localidad = ?", [clave_municipio_actual, clave_localidad_actual], function (tx, results) 
		        {
						if(results.rows.length > 0)
						    var extent = L.latLngBounds (L.latLng(results.rows.item(0).ymin, results.rows.item(0).xmin ), L.latLng(results.rows.item(0).ymax, results.rows.item(0).xmax));
						else
						    var extent = L.latLngBounds (L.latLng(municipios_extent_array[clave_municipio_actual+'_ymin'], municipios_extent_array[clave_municipio_actual+'_xmin'] ), L.latLng(municipios_extent_array[clave_municipio_actual+'_ymax'], municipios_extent_array[clave_municipio_actual+'_xmax']));
		 
		 				mapa_manzana_puntos.fitBounds(extent);

		        }, function (tx) {
		            alert('Error en el select');
		        });
			});	
}