	
	/*
		Charts - Interface for Flot library
		Version: 1.0
		Author: Thomas John (http://thomasjohn.eu), 2012
				
		Vars:
		
		piea - used by animation
		
		
		Functions / interface: 
	
		chart_update(id,data,maintype,type) - render chart
			
			id - div id
			
			data - array of data
				0,3:
				[][0] - x
				[][1] - y
				
				1,2,4,5:
				[] - value
				
			maintype - 
				0 - grid with lines, bars or points
				1 - pie
				2 - donut
				3 - grid with stacking
				4 - animated pie
				5 - animated pie - inverse
				
			type -				
				all maintypes:
				[][0] - label
				[][1] - color
				
				0:
				[][2][0] - lines
				[][2][1] - points
				[][2][2] - bars
				[][2][3] - fill
				
		chart_animpie(pn, value, inverse) - pie animation
	*/
	
	var piea;	
	
	function chart_update(id, data, maintype, type, mostrar_cuadro_valores) {
	
		var show_pie=show_donut=false;
        var stack=null;
        var fill=false;
        var show_legend = false;
        var show_label_series = false;
        var pie_inner=0.8;

    //param_muesta_valores = 0 no muestra ni la tabla de valores ni los porcentajes en el grafico
    //param_muesta_valores = 1 solo muestra la tabla de valores
    //param_muesta_valores = 2 solo muestra los porcentajes en el grafico
    //param_muesta_valores = 3 muestra la tabla de valores y los porcentajes en el grafico
    //param_muesta_valores = 4 muestra solo la tabla de valores con los porcentajes
    //param_muesta_valores = 5 muestra solo la tabla de valores con los porcentajes sin descripcion

        if(mostrar_cuadro_valores == 0)
        {
        	show_label_series = false;
        	pie_inner=0.8;
        	show_legend = false;
        }
        else if(mostrar_cuadro_valores == 1)
        {
        	show_label_series = false;
        	pie_inner=0.8;
        	show_legend = true;
        }
        else if(mostrar_cuadro_valores == 2)
        {
        	show_label_series = true;
        	pie_inner=0.6;
        	show_legend = false;
        }
        else if(mostrar_cuadro_valores == 3)
        {
        	show_label_series = true;
        	pie_inner=0.6;
        	show_legend = true;
        }
        else if((mostrar_cuadro_valores == 4)||(mostrar_cuadro_valores == 5))
        {
        	show_label_series = false;
        	pie_inner=0.8;
        	show_legend = true;
        }

		var sp=[];
		
		if (maintype==4 || maintype==5)	//anim
		{
	
			if (maintype==4){
			
				var i1 = 1;
				var i2 = 0.6;
			
				if (data[0]<0){
					i1=0.6;	//5
					i2=1;
				}
			}
			else{
				var i1 = 0.6;	//5
				var i2 = 0.25;
			
				if (data[0]<0){
					i1=0.25;
					i2=0.6;
				}
			}
			
			sp.push( {label: type[0][0], color: "rgba("+type[0][1]+", "+i1+")", data: data[0], lines: { show: false}, points: { show: false}, bars: { show: false} }); 
			
			sp.push( {label: type[1][0], color: "rgba("+type[1][1]+", "+i2+")", data: data[1], lines: { show: false}, points: { show: false}, bars: { show: false} }); 
		}
		else
			for(var i in data)
			{
				switch (maintype)
        		{
        			case 0:	//grid
        			fill=type[i][2][3];
        			break;
        		}
        		
        		
				if (maintype==1 || maintype==2)	//pie,donut
					sp.push( {label: type[i][0], color: "rgba("+type[i][1]+", 0.65)", data: data[i], lines: { show: false }, points: { show: false }, bars: { show: false } });
				else	//grid, grid with stacking
					sp.push( {label: type[i][0], color: "rgba("+type[i][1]+", 0.45)", data: data[i], lines: { show: type[i][2][0], fill: fill }, points: { show: type[i][2][1]}, bars: { show: type[i][2][2]} }); 
			}
		
        
        switch (maintype)
        {		
        	case 1:	//pie
        		show_pie=true;
        		break;
        	
        	case 2:	//donut
        	case 4:	//anim
        	case 5:
        		show_pie=true;
        		//pie_inner=0.6;
        		break;
        		
        	case 3:	//stacking
        		stack=true;
        }
        
        return $.plot($(id), 
        
        	sp,	

        	{
            
            series: {
                stack: stack,
                lines: { lineWidth:2, steps: false },
                bars: { lineWidth:0, fill:0.45, barWidth: 0.6, align:"center", horizontal: false },
                points: { lineWidth:2, fill:1, radius:3 },
                pie: { show: show_pie, stroke:{color:'rgba(255,255,255,1)', width:0.1}, lineWidth:0, fill:0.6, innerRadius: pie_inner,
                		label: {
                				show: show_label_series,
                				radius: 1,
                				threshold: 0.02,
                				formatter: labelFormatter,
            			}
        		},
                
                highlightColor: '#303030',
                shadowSize: 0         
            },
			legend: {
			    show: show_legend,
			    position: "ne"
			},

            xaxis: {
            	show:false
    		
    		},
    		
    		grid: {
    			borderWidth: 0,
    			margin: 20
    		},
    		
    		margin: 20
    		
        });
    }

	function labelFormatter(label, series) {
		if(label == '')
			return "<div style='font-size:8pt; color: white; text-align:center; padding:2px;'></div>";
		else
			return "<div style='font-size:8pt; color: "+series.color+"; text-align:center; padding:2px;'>" + Math.round(series.percent) + "%</div>";
			//return "<div style='font-size:8pt; color: "+series.color+"; text-align:center; padding:2px;'>" + label + " (" + Math.round(series.percent) + "%)</div>";
	}
    
    //param_muesta_valores = 0 no muestra ni la tabla de valores ni los porcentajes en el grafico
    //param_muesta_valores = 1 solo muestra la tabla de valores
    //param_muesta_valores = 2 solo muestra los porcentajes en el grafico
    //param_muesta_valores = 3 muestra la tabla de valores y los porcentajes en el grafico
    //param_muesta_valores = 4 muestra la tabla de valores con los graficos
    //param_muesta_valores = 5 muestra solo la tabla de valores con los porcentajes sin descripcion

    
    function chart_animpie2(id, datos_array_json, etiquetas_color_json, mostrar_cuadro_valores) 
	{
		var animacion = 0; //Si es 1 el grafico será animado, 0 para mostrar los datos de una sola vista

		var valores_array = $.parseJSON(datos_array_json);
		var etiquetas_color_array = $.parseJSON(etiquetas_color_json);
		var data = [];
		var type = [];

		var suma1 = 0;
		var suma2 = 0;
		var cont;
		//var muestra_porcen_grafico = 0;
		//var mostrar_cuadro_valores = 0;
		var valor_restante = 100;

/*		if(valores_array.length > 1)
			mostrar_cuadro_valores = 1;
		else
			mostrar_cuadro_valores = 0;
*/

		if(animacion == 0)
		{
				for (cont= 0; cont < valores_array.length; cont = cont + 1)
				{
					valores_array[cont][0] = valores_array[cont][1]
				}			
		}



		for (cont= 0; cont < valores_array.length; cont = cont + 1)
		{
			if(valores_array[cont][0] < valores_array[cont][1] )
			{
				data[cont] = valores_array[cont][0];
				suma1 += data[cont];
			}
			else
			{
				if(valores_array[cont][1] == 0)
					data[cont] = 0.2;
				else
					data[cont] = valores_array[cont][1];

					suma1 += data[cont];					
			}

			
			suma2 += valores_array[cont][1];

			valores_array[cont][0] = valores_array[cont][0] + 1;

/*			if(mostrar_cuadro_valores == 1)
				type[cont] = [ etiquetas_color_array[cont][0], etiquetas_color_array[cont][1] ];
			else
				type[cont] = ["", etiquetas_color_array[cont][1] ];
*/

/*			if(mostrar_cuadro_valores == 1)
				if(valores_array[cont][1] == 0)
					type[cont] = [ etiquetas_color_array[cont][0] + " (<" +data[cont]+ "%)", etiquetas_color_array[cont][1] ];
				else
					type[cont] = [ etiquetas_color_array[cont][0] + " (" +data[cont]+ "%)", etiquetas_color_array[cont][1] ];
			else
				type[cont] = ["", etiquetas_color_array[cont][1] ];

*/

    //param_muesta_valores = 0 no muestra ni la tabla de valores ni los porcentajes en el grafico
    //param_muesta_valores = 1 solo muestra la tabla de valores
    //param_muesta_valores = 2 solo muestra los porcentajes en el grafico
    //param_muesta_valores = 3 muestra la tabla de valores y los porcentajes en el grafico
    //param_muesta_valores = 4 muestra solo la tabla de valores con los porcentajes


			if(mostrar_cuadro_valores == 0)
			{
				type[cont] = ["", etiquetas_color_array[cont][1] ];
			}
			else if(mostrar_cuadro_valores == 1)
			{
				type[cont] = [ etiquetas_color_array[cont][0], etiquetas_color_array[cont][1] ];
			}
			else if(mostrar_cuadro_valores == 2)
			{
				type[cont] = [ etiquetas_color_array[cont][0], etiquetas_color_array[cont][1] ];
			}
			else if(mostrar_cuadro_valores == 3)
			{
				type[cont] = [ etiquetas_color_array[cont][0], etiquetas_color_array[cont][1] ];
			}
			else if(mostrar_cuadro_valores == 4)
			{
				if(valores_array[cont][1] == 0)
					type[cont] = [ etiquetas_color_array[cont][0] + " (< 0.1%)", etiquetas_color_array[cont][1] ];
				else
					type[cont] = [ etiquetas_color_array[cont][0] + " (" +data[cont]+ "%)", etiquetas_color_array[cont][1] ];				
			}
			else if(mostrar_cuadro_valores == 5)
			{
				if(valores_array[cont][1] == 0)
					type[cont] = [ "< 0.1%", etiquetas_color_array[cont][1] ];
				else
					type[cont] = [ data[cont]+ "%", etiquetas_color_array[cont][1] ];				
			}

		}

		data.push(valor_restante - suma1);
		type.push( ["", "210,210,210"] );

		chart_update("#"+id, data, 2, type, mostrar_cuadro_valores);	//render

/*		if(data.length <= 2)
			document.getElementById(id+"_value").innerHTML = data[0]+'%';
		else
			document.getElementById(id+"_value").innerHTML = '';
*/    	
		document.getElementById(id+"_value").innerHTML = '';
		
		if (suma1>=suma2)
		{
			
			if(data.length <= 2)
				document.getElementById(id+"_value").innerHTML = data[0]+'%';
			else
				document.getElementById(id+"_value").innerHTML = '';

			$('#'+id+'_value').addClass('text-success');
			return;
		}
				
		if(animacion == 1)
			setTimeout("chart_animpie2('"+id+"','"+JSON.stringify(valores_array)+"', '"+JSON.stringify(etiquetas_color_array)+"', "+mostrar_cuadro_valores+")", 0);
	}
/*	
	
    function chart_animpie(id, value, endvalue, inverse, rgb_foreground_color, rgb_background_color)
	{
		
		//endvalue = piea[pn][0];
		
		
		var data = [];		
		
		if (endvalue<0)
		{
			data[1]=value;		
			data[0]=-100-data[1];
		}
		else
		{
			data[0] = value;
			data[1] = 100-data[0];
		}
	
		
		var type = [];
		if (inverse){
			type[0] = ["",rgb_foreground_color];
			type[1] = ["",rgb_background_color]; 
			chart_update("#"+id, data, 2, type, muestra_porcen_grafico);	//render
		}
		else{
			type[0] = ["",rgb_foreground_color];
			type[1] = ["",rgb_background_color]; 	
			chart_update("#"+id, data, 4, type, muestra_porcen_grafico);	//render
    	}
    	
		if (value==endvalue)
		{
			
			//show value
			
			if (value>0)
				$('#'+id+'_value').addClass('text-success');
			else
				$('#'+id+'_value').addClass('text-error');
											
			return;
		}
		else
		{	
			if (endvalue>0)
				value++;	
			else
				value--;
		}
		
			document.getElementById(id+"_value").innerHTML=value+'%';
		
		setTimeout("chart_animpie('"+id+"',"+value+","+endvalue+","+inverse+",'"+rgb_foreground_color+"','"+rgb_background_color+"')", 20);
	}
	*/
	
	//end
	