function muestra_mapa_localidad(map_object, mapa_detalle, mostrar_simbologia, mostrar_texto_inicio)
{
	etiquetas_localidad_visible = false;
	var datos_localidades_array = new Array();

	//Esto es para mostrar datos del mapa
  	db.transaction(function (tx) {
        tx.executeSql("SELECT indicadore_id, datos_localidad_indicadores.clave_localidad, layer_localidades.nombre, layer_localidades.longitud, layer_localidades.latitud, SUM(valor) as valor , SUM(porcentaje) as porcentaje FROM datos_localidad_indicadores JOIN layer_localidades "+
        				"ON (datos_localidad_indicadores.clave_municipio = layer_localidades.clave_municipio AND datos_localidad_indicadores.clave_localidad = layer_localidades.clave_localidad ) "+
        				" WHERE indicadore_id = ? AND datos_localidad_indicadores.clave_municipio = ? AND valor > 0 GROUP BY indicadore_id, datos_localidad_indicadores.clave_localidad ORDER BY porcentaje DESC", [clave_indicador_actual, clave_municipio_actual], function (tx, results) 
        {
		            	if(results.rows.length > 0)
		            	{
				            	var i;
				            	for (i= 0; i < results.rows.length; i = i + 1)
				            	{
				            		datos_localidades_array.push('{"type": "Feature", "id": '+i+',  "properties": {"id": '+i+', "nombre": "'+results.rows.item(i).nombre+'", "valor": '+results.rows.item(i).valor+', "porcentaje": '+results.rows.item(i).porcentaje+'}, "geometry": { "type": "Point", "coordinates": ['+results.rows.item(i).longitud+', '+results.rows.item(i).latitud+'] } }'); 
		 		            	}

		 		            	var ValorSimbologiaMax = results.rows.item(0).porcentaje;
		 		            	var ValorSimbologiaMin = results.rows.item(results.rows.length - 1).porcentaje;
		 		            	var valorMax = results.rows.item(0).valor;
		 		            	var valorMin = results.rows.item(results.rows.length - 1).valor;

							    var data_localidades = JSON.parse('{"type": "FeatureCollection", "features": ['+ datos_localidades_array.toString() + ']}');

							    var extent = L.latLngBounds (L.latLng(municipios_extent_array[clave_municipio_actual+'_ymin'], municipios_extent_array[clave_municipio_actual+'_xmin'] ), L.latLng(municipios_extent_array[clave_municipio_actual+'_ymax'], municipios_extent_array[clave_municipio_actual+'_xmax']));

								map_object.fitBounds(extent);
								//map_object.zoomIn();

								setTimeout(function(){
									genera_simbolos_graduados(map_object, mapa_detalle, data_localidades, valorMin, valorMax, ValorSimbologiaMin, ValorSimbologiaMax, mostrar_simbologia, mostrar_texto_inicio);
								}, 500);		            		
		            	}

        }, function (tx) {
            alert('Error en el select');
        });
	});

}




function genera_simbolos_graduados(map_object, mapa_detalle, json_data, valorMin, valorMax, ValorSimbologiaMin, ValorSimbologiaMax, mostrar_simbologia, mostrar_texto_inicio)
{
    //Leemos cada elemento del json y creamos el punto con las coordenadas, luego le asignamos el tamaño de acuerdo al campo valor
	
	//if(map_object.hasLayer(puntos_graduados) == true)
	//	map_object.removeLayer(puntos_graduados);
		
	/*if(limpiar_todo == true)
	{
		if(puntos_graduados != undefined)
		{
			//puntos_graduados.eachLayer(function(layer) {  
				map_object.removeLayer(puntos_graduados);
				puntos_graduados.clearLayers();
			//});		
		}		
	}*/

	puntos_graduados = L.geoJson(json_data, {		

		pointToLayer: function(feature, latlng) {	

			if(mapa_detalle == true)
			{
				return L.circleMarker(latlng, {		
				
				    fillColor: "#FF3333",	
				    color: '#537898',	
				    weight: 1,	
				    fillOpacity: 0.6  

				}).bindLabel(feature.properties.nombre, {noHide: true, className: "txt_nombre_localidades", direction:'auto' })

			}
			else
			{
				return L.circleMarker(latlng, {		
				
				    fillColor: "#FF3333",	
				    color: '#537898',	
				    weight: 1,	
				    fillOpacity: 0.6  

				})				
			}
		}
	}).addTo(map_object);  

	puntos_graduados.eachLayer(function(layer) {  
		
		var props = layer.feature.properties,
			radius = calcPropRadius(props.porcentaje),
			popupContent = "<b> Total: " + String(props.valor) + "</b><br>" +
						   "<i>" + props.nombre + "</i>";

		layer.setRadius(radius);
		if(mapa_detalle == true)
		{
			layer.bindPopup(popupContent, { offset: new L.Point(0,-radius) }); 
			layer.on({

				mouseover: function(e) {
					this.openPopup();
					this.setStyle({color: 'yellow'});
				},
				mouseout: function(e) {
					this.closePopup();
					this.setStyle({color: '#537898'});
						
				}
			});			
		}

		/*
		var label = new L.Label()
		label.setContent(props.nombre)
		label.setLatLng(L.latLng(layer.feature.geometry.coordinates[1], layer.feature.geometry.coordinates[0]) )
		map_object.showLabel(label);
		*/
	});

	if(mostrar_simbologia == true)
		createLegend(map_object, valorMin, valorMax, ValorSimbologiaMin, ValorSimbologiaMax);

	if(mostrar_texto_inicio == false){
		etiquetas_localidad_visible = true;
		etiquetas_mapa_localidad_grande();
	}

}


function calcPropRadius(attributeValue) {
		
	var scaleFactor = 16,  // value dependent upon particular data set
		area = attributeValue * scaleFactor; 

	return Math.sqrt(area/Math.PI)*2;  
		
} // end calcPropRadius


function createLegend(map_object, valorMin, valorMax, ValorSimbologiaMin, ValorSimbologiaMax) {
	 
	if (ValorSimbologiaMin < 10) {	
		ValorSimbologiaMin = 10; 
	}
	function roundNumber(inNumber) {

   		return (Math.round(inNumber/10) * 10);  
	}

	var legend = L.control( { position: 'bottomright' } );

	legend.onAdd = function(map) {

		var legendContainer = L.DomUtil.create("div", "legend"),  
			symbolsContainer = L.DomUtil.create("div", "symbolsContainer"),
			classes = [roundNumber(ValorSimbologiaMin), roundNumber((ValorSimbologiaMax-ValorSimbologiaMin)/2), roundNumber(ValorSimbologiaMax)], 
			classesText = [roundNumber(valorMin), roundNumber((valorMax-valorMin)/2), roundNumber(valorMax)], 
			legendCircle,  
			diameter,
			diameters = [];  

		L.DomEvent.addListener(legendContainer, 'mousedown', function(e) { L.DomEvent.stopPropagation(e); });  

		$(legendContainer).append("<h2 id='legendTitle'>Simbologia</h2>");
		
		for (var i = 0; i < classes.length; i++) {  

			legendCircle = L.DomUtil.create("div", "legendCircle");  
			diameter = calcPropRadius(classes[i])*2; 
			diameters.push(diameter);
			
			var lastdiameter;
			
			if (diameters[i-1]){
				lastdiameter = diameters[i-1];
			} else {
				lastdiameter = 0;
			};
			$(legendCircle).attr("style", "width: "+diameter+"px; height: "+diameter+
				"px; margin-left: -"+((diameter+lastdiameter+2)/2)+"px" );

			
			$(legendCircle).append("<span class='legendValue'>"+number_format( classesText[i], 0, '.', ',' )+"<span>");

		
			$(symbolsContainer).append(legendCircle);	

		};

		$(legendContainer).append(symbolsContainer); 

		return legendContainer; 

	};

	legend.addTo(map_object);  
} // end createLegend()





function muestra_mapa_modal_localidades()
{
	var customModal = $('<div class="custom-modal modal hide fade" id="mapaModalLocalidades" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width: 700px; left: 10px; margin-left: 21px;">'+
						'  <div class="modal-dialog">'+
						'    <div class="modal-content">'+
						'      <div class="modal-header">'+
						'        <h4 class="modal-title" id="titulo_mapaModalLocalidades">'+titulo_indicador_actual+'</h4>'+
						'      </div>'+
						'      <div class="modal-body">'+
						'          <div id="mapa_modal_localidades" style="width: 100%; height: 700px; margin: 0 auto;"></div>'+
						'      </div>'+
						'      <div class="modal-footer">'+
						'        <button type="button" class="btn btn-default pull-left" onclick="etiquetas_mapa_localidad_grande()">Mostrar/ocultar etiquetas</button>'+
						'        <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Cerrar</button>'+
						'      </div>'+
						'    </div>'+
						'  </div>'+
						'</div>');

	  //var customModal = $('<div class="custom-modal modal hide fade" tabindex="-1" role="dialog" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button></div><div class="modal-body"></div><div class="modal-footer"><button class="btn" data-dismiss="modal">Close</button></div></div>');

	    $('body').append(customModal);
	    $(this).find($('h3')).clone().appendTo('.custom-modal .modal-header');
	    $(this).find('.device-product, .device-details').clone().appendTo('.custom-modal .modal-body');
	    $('.custom-modal .hide').show();
	    $('.custom-modal').modal();
	  
	    var map = L.map('mapa_modal_localidades',{ zoomControl:false });

	  	$('.custom-modal').on('hidden', function(){
			map.eachLayer(function (layer) {
				    map.removeLayer(layer);
			});	  		
	 		console.log("cerrando modal");
	    	$('.custom-modal').remove();
		});
		$('.custom-modal').on('shown', function(){ 

			    map.setView([19.4614619, -88.300083], 6);
			    map.addLayer(layer_street2);

			    var geojsonLayer = L.geoJson(limites_estatales_municipales, {        
			        style: {
			            "clickable": false,
			            "color": "#AAAAAA",
			            "weight": 5.0,
			            "opacity": 0.9,
			            "strokeDashstyle": "dash",
			        }
			    }).addTo(map);  

				muestra_mapa_localidad(map, mapa_detalle=true, mostrar_simbologia=true, mostrar_texto_inicio=false);  

		});
}


function etiquetas_mapa_localidad_grande()
{
	if(etiquetas_localidad_visible == true)
	{
		puntos_graduados.eachLayer(function (marker) {
		    marker.hideLabel();
		    etiquetas_localidad_visible = false;
		});		
	}
	else
	{
		puntos_graduados.eachLayer(function (marker) {
		    marker.showLabel();
		    etiquetas_localidad_visible = true;
		});		
	}

}