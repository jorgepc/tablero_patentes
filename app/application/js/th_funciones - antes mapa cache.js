/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/* VARIABLES GLOBALES*/

var clave_indicador_actual = 1;
var clave_municipio_actual = '';
var clave_localidad_actual = '';
var titulo_indicador_actual = '';
var localidad_actual = '';
var id_grafico_municipio_actual = -1;
var id_grafico_localidad_actual = -1;

var vista_anterior = 'municipio';
var vista_actual = 'municipio';

var lista_id_valor_graficos_municipio = new Array();
var lista_id_valor_graficos_localidad = new Array();

var colores = [];
colores.push("0,0,255"); //Estado
colores.push("0,147,0"); //Municipio1
colores.push("255,128,64"); //Municipio2
colores.push("255,0,255"); //Municipio3
colores.push("0,255,0"); //Municipio4
colores.push("50,117,158"); //Municipio5
colores.push("0,255,255"); //Municipio6
colores.push("255,128,0"); //Municipio7
colores.push("128,0,255"); //Municipio8
colores.push("255,128,255"); //Municipio9
colores.push("255,255,0"); //Municipio10

var mapa_localidad_referencia;
var mapa_manzana_puntos;
var layer_street;
var layer_satelite;
var puntos_graduados;
var puntos_patentes;
var heatmapLayer;
var etiquetas_localidad_visible;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

(function (emr) {
    emr.on('storageLoaded', 'mapLoad');
    emr.fire('storageLoad');
    emr.on('storageLoaded2', 'mapLoad2');
    emr.fire('storageLoad2');
})(window.offlineMaps.eventManager);


function actualiza_menu_indicadores()
{
	//$("#contenedor_municipio").load( "application/plantillas/vista_municipios.html");
	//$("#contenedor_localidad").load( "application/plantillas/vista_localidad.html");

	$("#menu_indicadores").empty();

      db.transaction(function (tx) {
            tx.executeSql("SELECT * FROM indicadores ORDER BY indicadore_id ASC", [], function (tx, results) 
            {
                  if (results.rows && results.rows.length) 
                  {
		                  for (i= 0; i < results.rows.length; i = i + 1) {
		                        $("#menu_indicadores").append('<li id="mid'+results.rows.item(i).indicadore_id+'" onclick="cambia_indicador_menu('+results.rows.item(i).indicadore_id+')" class="leftmenu-opt"><a class="more" href="#"> <div style="display:inline-block;line-height:30px;margin-top:10px;margin-bottom:10px;"> <table border=0 cellpadding=0 cellspacing=0><tr><td width=25><i class="icon-th icon-white"></i></td><td>'+results.rows.item(i).nombre+'</td></tr></table></div></a></li>');
		                  }
                  }
                  else
                  {
		                        $("#menu_indicadores").append('<li class="leftmenu-opt"><a class="more" href="#"><i class="icon-th icon-white"></i>No hay indicadores<span id="badge_lm1" class="leftmenu-badge hide"></span></a></li>');
                  }
            }, function (tx) {
                alert('Error al cargar la lista de notificaciones de la ruta');
            });
      });

	mapa_localidad_referencia = L.map('mapa_localidades',{ zoomControl:false });
	mapa_localidad_referencia.dragging.disable();
	mapa_localidad_referencia.touchZoom.disable();
	mapa_localidad_referencia.doubleClickZoom.disable();
	mapa_localidad_referencia.scrollWheelZoom.disable();

	// Disable tap handler, if present.
	if (mapa_localidad_referencia.tap) mapa_localidad_referencia.tap.disable();					    	

	mapa_localidad_referencia.setView([19.4614619, -88.300083], 6);


	L.tileLayer('https://{s}.tiles.mapbox.com/v3/{id}/{z}/{x}/{y}.png', {
	    maxZoom: 9,
	    id: 'examples.map-i875mjb7',
	    detectRetina: true
	}).addTo(mapa_localidad_referencia);

	/************* mapa a nivel urbano **********************/
	mapa_manzana_puntos = L.map('mapa_manzana',{ zoomControl:true });
	mapa_manzana_puntos.setView([19.4614619, -88.300083], 6);

	layer_street = L.tileLayer('https://{s}.tiles.mapbox.com/v3/{id}/{z}/{x}/{y}.png', {
	    maxZoom: 20,
	    id: 'examples.map-i875mjb7',
	    detectRetina: true
	}).addTo(mapa_manzana_puntos);

/*	var Esri_WorldStreetMap = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}', {
		attribution: 'Tiles &copy; Esri &mdash; Source: Esri, DeLorme, NAVTEQ, USGS, Intermap, iPC, NRCAN, Esri Japan, METI, Esri China (Hong Kong), Esri (Thailand), TomTom, 2012'
	});
*/
	layer_satelite = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
		attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
	});

	//mapa_manzana_puntos.addLayer(layer_satelite);

	mapa_manzana_puntos.on('moveend', function () {
		    if (mapa_manzana_puntos.getZoom() < 16 && mapa_manzana_puntos.hasLayer(heatmapLayer)==false) {
					mapa_manzana_puntos.addLayer(heatmapLayer);
		    }
		    if (mapa_manzana_puntos.getZoom() >= 16 && mapa_manzana_puntos.hasLayer(heatmapLayer)) {
		       		mapa_manzana_puntos.removeLayer(heatmapLayer);
		    }

		    if (mapa_manzana_puntos.getZoom() >= 16 && mapa_manzana_puntos.hasLayer(puntos_patentes)==false) {
		       mapa_manzana_puntos.addLayer(puntos_patentes);
		    }
		    if (mapa_manzana_puntos.getZoom() < 16 && mapa_manzana_puntos.hasLayer(puntos_patentes)) {
		       mapa_manzana_puntos.removeLayer(puntos_patentes);
		    }

		    if (mapa_manzana_puntos.getZoom() >= 18 && mapa_manzana_puntos.hasLayer(layer_satelite))
		       mapa_manzana_puntos.removeLayer(layer_satelite);

	    });


	var cfg = {
	  // radius should be small ONLY if scaleRadius is true (or small radius is intended)
	  "radius": 9,
	  "maxOpacity": .8, 
	  // scales the radius based on map zoom
	  "scaleRadius": false, 
	  // if set to false the heatmap uses the global maximum for colorization
	  // if activated: uses the data maximum within the current map boundaries 
	  //   (there will always be a red spot with useLocalExtremas true)
	  "useLocalExtrema": true,
	  // which field name in your data represents the latitude - default "lat"
	  latField: 'latitud',
	  // which field name in your data represents the longitude - default "lng"
	  lngField: 'longitud',
	  // which field name in your data represents the data value - default "value"
	  valueField: 'total'
	};

	puntos_patentes = new L.FeatureGroup();
	heatmapLayer = new HeatmapOverlay(cfg);
    
    //map_object.addLayer(puntos_patentes);
    mapa_manzana_puntos.addLayer(heatmapLayer);



}


function controlador_vista_tablero(indicadore_id)
{
	//return;
	clave_indicador_actual = indicadore_id;

	if(vista_actual == 'municipio')
		muestra_datos_municipio();
	else if(vista_actual == 'localidad')
		vista_datos_localidad()
	else if(vista_actual == 'manzana')
		prepara_mapa_manzana();
}


function cambia_indicador_menu(indicadore_id)
{
		//Este codigo es solamente para deseleccionar el indicador seleccionado
		db.transaction(function (tx) {
		    tx.executeSql("SELECT * FROM indicadores", [], function (tx, results) 
		    {
		    	for (i= 0; i < results.rows.length; i = i + 1)
		    	{
		        	if(results.rows.item(i).indicadore_id == clave_indicador_actual)
		        	{
		              $("#txt_titulo_indicador").html(results.rows.item(i).nombre);
		              titulo_indicador_actual = results.rows.item(i).nombre;
		              $("#txt_descripcion_indicador").html("Descripci&oacute;n: " + results.rows.item(i).descripcion);
		              $('#mid'+results.rows.item(i).indicadore_id).addClass('leftmenu-activeopt');            		
		        	}
		        	else
		        	{
		              $('#mid'+results.rows.item(i).indicadore_id).removeClass('leftmenu-activeopt');            		
		        	}
		    	}

		    }, function (tx) {
		        alert('Error en el select');
		    });
		});

		    	controlador_vista_tablero(indicadore_id);

}

function muestra_menu_otros_municipios() //borrar
{
	$("#myModal").modal('show');

}


function muestra_datos_localidad(id_grafico)
{
		if(id_grafico != -1)
			id_grafico_municipio_actual = id_grafico;
		
		vista_actual = 'localidad';
		clave_municipio_actual = lista_id_valor_graficos_municipio[id_grafico_municipio_actual];
		controlador_vista_tablero(clave_indicador_actual);
}

function muestra_datos_manzana(id_grafico)
{
		id_grafico_localidad_actual = id_grafico;
		vista_actual = 'manzana';
		clave_localidad_actual = lista_id_valor_graficos_localidad[id_grafico_localidad_actual];
		controlador_vista_tablero(clave_indicador_actual);
}



function muestra_datos_municipio()
{	

/*	mapa_localidad_referencia.eachLayer(function (layer) {
	    mapa_localidad_referencia.removeLayer(layer);
	});

	L.tileLayer('https://{s}.tiles.mapbox.com/v3/{id}/{z}/{x}/{y}.png', {
	    maxZoom: 18,
	    id: 'examples.map-i875mjb7',
	    detectRetina: true
	}).addTo(mapa_localidad_referencia);
*/

      vista_actual = 'municipio';
      id_grafico_municipio_actual = -1;
      clave_municipio_actual = '';
//      $("#contenedor_vistas").load( "application/plantillas/vista_municipios.html", function() {

				//$("#tabla_municipios").empty();

				var datos_array = new Array();
				var datos_texto_array = new Array();
				var textos_color_array = new Array();
				lista_id_valor_graficos_municipio.length = 0;

				db.transaction(function (tx) {
				    tx.executeSql("SELECT MIN(elemento) as elemento FROM datos_municipio_indicadores WHERE indicadore_id = ?", [clave_indicador_actual], function (tx, results) 
				    {

						if(results.rows.item(0).elemento == 'unico')
						{
					          //$("#encabezado_tabla_datos").empty();
					          //$("#encabezado_tabla_datos").append('<tr><th colspan="2">El Estado y sus municipios</th></tr>');
					          $("#div_tabla_datos").html('<table class="table table-conddensed margin-0"><tbody id="tabla_municipios" style="font-size: 12px;"></tbody></table>');
						      
						      db.transaction(function (tx) {
						            tx.executeSql("SELECT * FROM datos_municipio_indicadores WHERE indicadore_id = ? ORDER BY porcentaje DESC", [clave_indicador_actual], function (tx, results) 
						            {
									            	var i;
									            	for (i= 0; i < results.rows.length; i = i + 1)
									            	{

											            		if(results.rows.item(i).municipio == 'QUINTANA ROO')
											            			$('#tabla_municipios').append('<tr><td><div style="width:4px;height:0;border:5px solid rgba('+colores[i]+', 0.65);overflow:hidden"></div></td><td>'+results.rows.item(i).municipio+'</td><td>'+number_format( results.rows.item(i).valor, 0, '.', ',' )+'</td></tr><tr><td></td><td></td><td></td></tr>');
											            		else
											            			$('#tabla_municipios').append('<tr><td><div style="width:4px;height:0;border:5px solid rgba('+colores[i]+', 0.65);overflow:hidden"></div></td><td>'+results.rows.item(i).municipio+'</td><td>'+number_format( results.rows.item(i).valor, 0, '.', ',' )+'</td></tr>');

												            		$("#chart-top"+i+"_vframe").html(results.rows.item(i).municipio);
																	datos_array.length = 0;
																	textos_color_array.length = 0;
																	datos_array[0] = [0, parseFloat(results.rows.item(i).porcentaje.toFixed(2))];
																	textos_color_array[0] = ["", colores[i]]; 
												            		//chart_animpie("chart-top"+(i+1), 0, results.rows.item(i).porcentaje.toFixed(), true, colores[i], "190,190,190");
												            		chart_animpie2("chart-top"+i, JSON.stringify(datos_array) , JSON.stringify(textos_color_array), 0 );

												            		lista_id_valor_graficos_municipio[i] = results.rows.item(i).clave_municipio; 

																	datos_array.length = 0;
																	textos_color_array.length = 0;
									            	}
									            	//$("#chart-top5_vframe").html("Resto de los municipios");
									            	

									            	//$("#chart-top5").width(400);
									            	//chart_animpie2("chart-top5", JSON.stringify(datos_array) , JSON.stringify(textos_color_array), 4 );
									            	$('#cuadro_simbologia').html('<span style="font-size:15px; top: 1px">Resto de <br>los municipios</span>');
						
						            }, function (tx) {
						                alert('Error en el select');
						            });
								});
				    	}
						else
						{
					          //$("#encabezado_tabla_datos").empty();
					          //$("#encabezado_tabla_datos").append('<tr><th colspan="3">El Estado y sus municipios</th></tr>');
					          $("#div_tabla_datos").html('<table class="table table-conddensed margin-0"><tbody id="tabla_municipios" style="font-size: 12px;"></tbody></table>');
						      db.transaction(function (tx) {
						            tx.executeSql("SELECT datos_municipio_indicadores.*, x.total FROM datos_municipio_indicadores JOIN "+
						            			  "(SELECT indicadore_id, clave_municipio, SUM(valor) as total FROM datos_municipio_indicadores GROUP BY indicadore_id, clave_municipio) as x "+
						            			  "ON (datos_municipio_indicadores.indicadore_id = x.indicadore_id AND datos_municipio_indicadores.clave_municipio = x.clave_municipio) "+
						            			  "WHERE datos_municipio_indicadores.indicadore_id = ? ORDER BY x.total DESC, datos_municipio_indicadores.clave_municipio ASC, datos_municipio_indicadores.elemento ASC", [clave_indicador_actual], function (tx, results) 
						            {
						            		if(results.rows.length > 0)
						            		{
									            	var i;
									            	var j = 0;
									            	var i_colores = 0;
									            	var municipio_actual = results.rows.item(0).municipio;
									            	var elemento = '';
									            	var codigo = '';
									            	var codigo_total = '';
									            	var codigo_tabla_simbologia = '<div style="font-size:18px;color:#545454;margin-bottom:5px;">Simbolog&iacute;a</div> <table><tbody>';
									            	lista_id_valor_graficos_municipio.push(results.rows.item(0).clave_municipio);

									            	for (i= 0; i < results.rows.length; i = i + 1)
									            	{

														        if(results.rows.item(i).municipio == 'QUINTANA ROO')
														        {
														        	codigo_tabla_simbologia += '<tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px">'+
														        							   '<div style="width:8px;height:0;border:9px solid rgba('+colores[i_colores]+', 0.65);overflow:hidden"></div></div>'+
				                                                                               '</td><td class="legendLabel" style="font-size: 12px;">'+results.rows.item(i).elemento+'</td></tr>';
														        }

											            		//if(j < 5)
											            		//{
													            		if(municipio_actual != results.rows.item(i).municipio)
													            		{
														            		$("#chart-top"+j+"_vframe").html(municipio_actual);
														            		chart_animpie2("chart-top"+j, JSON.stringify(datos_array) , JSON.stringify(textos_color_array), 5 );
														            		codigo_total += '<tr><td valign="middle" rowspan="'+datos_array.length+'">'+codigo.substr(0, codigo.length - 4) ;
														            		codigo = '';

														            		codigo_total += '</tr><tr><td></td><td></td><td></td><td></td></tr>';

																			datos_array.length = 0;
																			textos_color_array.length = 0;
																			i_colores = 0;
																			lista_id_valor_graficos_municipio.push(results.rows.item(i).clave_municipio);

																			datos_array.push( [0, parseFloat(results.rows.item(i).porcentaje.toFixed(2))] );
																			textos_color_array.push( [results.rows.item(i).elemento, colores[i_colores]] );
																			municipio_actual = results.rows.item(i).municipio;
																			j++;
													            		}            			
													            		else
													            		{            		
																			datos_array.push( [0, parseFloat(results.rows.item(i).porcentaje.toFixed(2))] );
																			textos_color_array.push( [results.rows.item(i).elemento, colores[i_colores]] );
													            		}
											            		/*}
											            		else
											            		{

													            		if(municipio_actual != results.rows.item(i).municipio)
													            		{
														            		codigo_total += '<tr><td rowspan="'+datos_array.length+'">'+codigo.substr(0, codigo.length - 4) ;
														            		codigo = '';
														            		codigo_total += '</tr><tr><td></td><td></td><td></td></tr>';

																			datos_array.length = 0;
																			i_colores = 0;
																			datos_array.push( [0, parseFloat(results.rows.item(i).porcentaje.toFixed(2))] );
																			municipio_actual = results.rows.item(i).municipio;
													            		}            			
													            		else
													            		{            		
																			datos_array.push( [0, parseFloat(results.rows.item(i).porcentaje.toFixed(2))] );
													            		}

																	if(datos_texto_array[results.rows.item(i).elemento] == undefined)
																		datos_texto_array[results.rows.item(i).elemento] = parseFloat(results.rows.item(i).porcentaje.toFixed(2) );
																	else
																		datos_texto_array[results.rows.item(i).elemento] = parseFloat(datos_texto_array[results.rows.item(i).elemento]) + parseInt(results.rows.item(i).porcentaje.toFixed(2) );

																	j++;
																	//datos_texto_array.push([new Array('elemento',results.rows.item(i).elemento), new Array('valor', parseInt(results.rows.item(i).porcentaje.toFixed() )  ) ] );
											            		}*/

											            		if(elemento != results.rows.item(i).municipio)
											            		{
											            			codigo += results.rows.item(i).municipio+'</td>';
											            			elemento = results.rows.item(i).municipio;
											            		}
											            		
											            		codigo += '<td><div style="width:4px;height:0;border:5px solid rgba('+colores[i_colores]+', 0.65);overflow:hidden"></div></td>';
											            		codigo += '<td>'+results.rows.item(i).elemento+'</td>'+'<td>'+number_format( results.rows.item(i).valor, 0, '.', ',' )+'</td></tr>';
																codigo += '<tr>';

											            		i_colores++;

																if(i_colores > 10)
																	i_colores = 0;

									            	}

									            	codigo_total += '<tr><td rowspan="'+datos_array.length+'">'+codigo.substr(0, codigo.length - 4) ;
									            	$("#div_tabla_datos").html('<table class="table table-conddensed margin-0"><tbody id="tabla_municipios" style="font-size: 12px;">'+codigo_total+'</tbody></table>');
									            	//$('#tabla_municipios').html(codigo);

													/*datos_array.length = 0;
													textos_color_array.length = 0;
													i_colores = 0;
													var arr = [];

													for(property in datos_texto_array) {
														arr.push(property);
													    //console.log(property + " = " + datos_texto_array[property]);
													}

													for (var n=arr.length; n--; ){
													   var valor = datos_texto_array[arr[n]];

														datos_array.push( [0, parseFloat(valor.toFixed(2) )] );
														textos_color_array.push( [arr[n], colores[i_colores]] );
														i_colores++;
														if(i_colores > 10)
															i_colores = 0;
													}
													$("#chart-top5").width(250);
									            	chart_animpie2("chart-top5", JSON.stringify(datos_array) , JSON.stringify(textos_color_array), 5 );*/

								            		$("#chart-top10_vframe").html(municipio_actual);
								            		chart_animpie2("chart-top"+j, JSON.stringify(datos_array) , JSON.stringify(textos_color_array), 5 );

									            	$('#cuadro_simbologia').html(codigo_tabla_simbologia+'</tbody></table>');
									            	//$('#cuadro_simbologia').html('<table style="position:absolute;top:5px;right:5px;;font-size:smaller;color:#545454"><thead><tr><th colspan="2" style="font-size:18px;color:#545454">Simbolog&iacute;a</th></tr></thead><tbody><tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid rgba(0,0,255, 0.65);overflow:hidden"></div></div></td><td class="legendLabel">10%</td></tr><tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid rgba(0,147,0, 0.65);overflow:hidden"></div></div></td><td class="legendLabel">3%</td></tr></tbody></table>');
											}

						            }, function (tx) {
						                alert('Error en el select');
						            });
								});
				    	}


				    }, function (tx) {
				        alert('Error en el select');
				    });
				});

      //}); //callback al cargar la vista

}



function vista_datos_localidad()
{

	mapa_localidad_referencia.eachLayer(function (layer) {
	    mapa_localidad_referencia.removeLayer(layer);
	});

	L.tileLayer('https://{s}.tiles.mapbox.com/v3/{id}/{z}/{x}/{y}.png', {
	    maxZoom: 18,
	    id: 'examples.map-i875mjb7',
	    detectRetina: true
	}).addTo(mapa_localidad_referencia);


	$("#div_toploc1").hide();
	$("#div_toploc2").hide();
	$("#div_toploc3").hide();
	$("#div_toploc4").hide();
	//clave_municipio_actual = lista_id_valor_graficos_municipio[id_grafico];

	//$("#contenedor_vistas").load( "application/plantillas/vista_localidad.html", function() {

			var datos_array = new Array();
			var datos_texto_array = new Array();
			var textos_color_array = new Array();
			lista_id_valor_graficos_localidad.length = 0;

			db.transaction(function (tx) {
			    tx.executeSql("SELECT MIN(elemento) as elemento FROM datos_localidad_indicadores WHERE indicadore_id = ? AND clave_municipio = ?", [clave_indicador_actual, clave_municipio_actual], function (tx, results) 
			    {

					if(results.rows.item(0).elemento == 'unico')
					{
				          $("#div_tabla_datos_localidad").html('<table class="table table-conddensed margin-0"><tbody id="tabla_localidades" style="font-size: 12px;"></tbody></table>');
					      
					      db.transaction(function (tx) {
					            tx.executeSql("SELECT * FROM datos_localidad_indicadores WHERE indicadore_id = ? AND clave_municipio = ? ORDER BY porcentaje DESC", [clave_indicador_actual, clave_municipio_actual], function (tx, results) 
					            {
								            	var i, j=0, resto_localidades = 0.0;
								            	for (i= 0; i < results.rows.length; i = i + 1)
								            	{

										            		$('#tabla_localidades').append('<tr><td><div style="width:4px;height:0;border:5px solid rgba('+colores[i]+', 0.65);overflow:hidden"></div></td><td>'+results.rows.item(i).localidad+'</td><td>'+number_format( results.rows.item(i).valor, 0, '.', ',' )+'</td></tr>');

										            		if(i >= 5)
										            		{            		
																resto_localidades += parseFloat(results.rows.item(i).porcentaje);
										            		}
										            		else
										            		{
											            		$("#div_toploc"+j).show();
											            		$("#chart-toploc"+j+"_vframe").html(results.rows.item(i).localidad);
																datos_array.length = 0;
																textos_color_array.length = 0;
																datos_array[0] = [0, parseFloat(results.rows.item(i).porcentaje.toFixed(2))];
																textos_color_array[0] = ["", colores[i]]; 
											            		//chart_animpie("chart-top"+(i+1), 0, results.rows.item(i).porcentaje.toFixed(), true, colores[i], "190,190,190");
											            		chart_animpie2("chart-toploc"+j, JSON.stringify(datos_array) , JSON.stringify(textos_color_array), 0 );

											            		lista_id_valor_graficos_localidad[i] = results.rows.item(i).clave_localidad; 
											            		j++;
																datos_array.length = 0;
																textos_color_array.length = 0;
										            		}

								            	}

								            	if( j < results.rows.length)
								            	{
													datos_array.length = 0;
													textos_color_array.length = 0;
									            	datos_array.push( [0, parseFloat(resto_localidades.toFixed(2) )] );
													textos_color_array.push( ['', colores[j]] );
									            	//$("#chart-toploc5").width(400);
									            	$("#chart-toploc4_vframe").html('Resto de las localidades');
									            	chart_animpie2("chart-toploc4", JSON.stringify(datos_array) , JSON.stringify(textos_color_array), 0 );
								            	}
								            	$('#cuadro_simbologia_localidad').html('');								            		
					
					            }, function (tx) {
					                alert('Error en el select');
					            });
							});
			    	}
					else
					{
				          $("#div_tabla_datos_localidad").html('<table class="table table-conddensed margin-0"><tbody id="tabla_localidades" style="font-size: 12px;"></tbody></table>');
					      db.transaction(function (tx) {
					            tx.executeSql("SELECT datos_localidad_indicadores.*, x.total FROM datos_localidad_indicadores JOIN "+
					            			  "(SELECT indicadore_id, clave_municipio, clave_localidad, SUM(valor) as total FROM datos_localidad_indicadores GROUP BY indicadore_id, clave_municipio, clave_localidad) as x "+
					            			  "ON (datos_localidad_indicadores.indicadore_id = x.indicadore_id AND datos_localidad_indicadores.clave_municipio = x.clave_municipio AND datos_localidad_indicadores.clave_localidad = x.clave_localidad) "+
					            			  "WHERE datos_localidad_indicadores.indicadore_id = ? AND datos_localidad_indicadores.clave_municipio = ? ORDER BY x.total DESC, datos_localidad_indicadores.clave_localidad ASC, datos_localidad_indicadores.elemento ASC", [clave_indicador_actual, clave_municipio_actual], function (tx, results) 
					            {
					            		if(results.rows.length > 0)
					            		{
								            	var i;
								            	var j = 0;
								            	var i_colores = 0;
								            	var localidad_actual = results.rows.item(0).localidad;
								            	var elemento = '';
								            	var codigo = '';
								            	var codigo_total = '';
								            	var codigo_tabla_simbologia = '<div style="font-size:18px;color:#545454;margin-bottom:5px;">Simbolog&iacute;a</div> <table><tbody>';
								            	lista_id_valor_graficos_localidad.push(results.rows.item(0).clave_localidad);

								            	for (i= 0; i < results.rows.length; i = i + 1)
								            	{

										            		if(j < 5)
										            		{
												            		if(localidad_actual != results.rows.item(i).localidad)
												            		{
													            		$("#div_toploc"+j).show();
													            		$("#chart-toploc"+j+"_vframe").html(localidad_actual);
													            		chart_animpie2("chart-toploc"+j, JSON.stringify(datos_array) , JSON.stringify(textos_color_array), 5 );
													            		codigo_total += '<tr><td valign="middle" rowspan="'+datos_array.length+'">'+codigo.substr(0, codigo.length - 4) ;
													            		codigo = '';

													            		codigo_total += '</tr><tr><td></td><td></td><td></td><td></td></tr>';

																		datos_array.length = 0;
																		textos_color_array.length = 0;
																		i_colores = 0;
																		lista_id_valor_graficos_localidad.push(results.rows.item(i).clave_localidad);

																		datos_array.push( [0, parseFloat(results.rows.item(i).porcentaje.toFixed(2))] );
																		textos_color_array.push( [results.rows.item(i).elemento, colores[i_colores]] );
																		localidad_actual = results.rows.item(i).localidad;
																		j++;
												            		}            			
												            		else
												            		{            		
																		datos_array.push( [0, parseFloat(results.rows.item(i).porcentaje.toFixed(2))] );
																		textos_color_array.push( [results.rows.item(i).elemento, colores[i_colores]] );
												            		}
										            		}
										            		else
										            		{

												            		if(localidad_actual != results.rows.item(i).localidad)
												            		{
													            		codigo_total += '<tr><td rowspan="'+datos_array.length+'">'+codigo.substr(0, codigo.length - 4) ;
													            		codigo = '';
													            		codigo_total += '</tr><tr><td></td><td></td><td></td></tr>';

																		datos_array.length = 0;
																		i_colores = 0;
																		datos_array.push( [0, parseFloat(results.rows.item(i).porcentaje.toFixed(2))] );
																		localidad_actual = results.rows.item(i).localidad;
												            		}            			
												            		else
												            		{            		
																		datos_array.push( [0, parseFloat(results.rows.item(i).porcentaje.toFixed(2))] );
												            		}

																if(datos_texto_array[results.rows.item(i).elemento] == undefined)
																	datos_texto_array[results.rows.item(i).elemento] = parseFloat(results.rows.item(i).porcentaje.toFixed(2) );
																else
																	datos_texto_array[results.rows.item(i).elemento] = parseFloat(datos_texto_array[results.rows.item(i).elemento]) + parseFloat(results.rows.item(i).porcentaje.toFixed(2) );

																j++;
																//datos_texto_array.push([new Array('elemento',results.rows.item(i).elemento), new Array('valor', parseInt(results.rows.item(i).porcentaje.toFixed() )  ) ] );
										            		}

													        if(j == 0)
													        {
													        	codigo_tabla_simbologia += '<tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px">'+
													        							   '<div style="width:8px;height:0;border:9px solid rgba('+colores[i_colores]+', 0.65);overflow:hidden"></div></div>'+
			                                                                               '</td><td class="legendLabel" style="font-size: 12px;">'+results.rows.item(i).elemento+'</td></tr>';
													        }

										            		if(elemento != results.rows.item(i).localidad)
										            		{
										            			codigo += results.rows.item(i).localidad+'</td>';
										            			elemento = results.rows.item(i).localidad;
										            		}
										            		
										            		codigo += '<td><div style="width:4px;height:0;border:5px solid rgba('+colores[i_colores]+', 0.65);overflow:hidden"></div></td>';
										            		codigo += '<td>'+results.rows.item(i).elemento+'</td>'+'<td>'+number_format( results.rows.item(i).valor, 0, '.', ',' )+'</td></tr>';
															codigo += '<tr>';

										            		i_colores++;

															if(i_colores > 10)
																i_colores = 0;

								            	}
												
								            	codigo_total += '<tr><td rowspan="'+datos_array.length+'">'+codigo.substr(0, codigo.length - 4) ;
								            	$("#div_tabla_datos_localidad").html('<table class="table table-conddensed margin-0"><tbody id="tabla_localidades" style="font-size: 12px;">'+codigo_total+'</tbody></table>');

												$("#chart-toploc4_vframe").html(localidad_actual);
												if( j >= 5)
												{
														$("#div_toploc4").show();
														$("#chart-toploc4_vframe").html('Resto de las localidades');
														datos_array.length = 0;
														textos_color_array.length = 0;
														i_colores = 0;
														var arr = [];

														for(property in datos_texto_array) {
															arr.push(property);
														    //console.log(property + " = " + datos_texto_array[property]);
														}

														for (var n=arr.length; n--; ){
														   var valor = datos_texto_array[arr[n]];

															datos_array.push( [0, parseFloat(valor.toFixed(2) )] );
															textos_color_array.push( [arr[n], colores[i_colores]] );
															i_colores++;
															if(i_colores > 10)
																i_colores = 0;
														}
												}

												//$("#chart-toploc4").width(250);
								            	chart_animpie2("chart-toploc4", JSON.stringify(datos_array) , JSON.stringify(textos_color_array), 5 );
								            	$('#cuadro_simbologia_localidad').html(codigo_tabla_simbologia+'</tbody></table>');
								            	//$('#cuadro_simbologia').html('<table style="position:absolute;top:5px;right:5px;;font-size:smaller;color:#545454"><thead><tr><th colspan="2" style="font-size:18px;color:#545454">Simbolog&iacute;a</th></tr></thead><tbody><tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid rgba(0,0,255, 0.65);overflow:hidden"></div></div></td><td class="legendLabel">10%</td></tr><tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid rgba(0,147,0, 0.65);overflow:hidden"></div></div></td><td class="legendLabel">3%</td></tr></tbody></table>');
										}

					            }, function (tx) {
					                alert('Error en el select');
					            });
							});
			    	}


			    }, function (tx) {
			        alert('Error en el select');
			    });
			});


			//Funcion para mostrar el mapa a nivel localidad
			muestra_mapa_localidad(mapa_localidad_referencia, mapa_detalle=false, mostrar_simbologia=false, mostrar_texto_inicio=false);




	//}); //callback al cargar la vista

}

