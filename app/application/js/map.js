(function (window, emr, L, undefined) {
    var StorageTileLayer = L.TileLayer.extend({
        _imageToDataUri: function (image) {
            var canvas = window.document.createElement('canvas');
            canvas.width = image.naturalWidth || image.width;
            canvas.height = image.naturalHeight || image.height;

            var context = canvas.getContext('2d');
            context.drawImage(image, 0, 0);

            return canvas.toDataURL('image/png');
        },

        _tileOnLoadWithCache: function () {
            var storage = this._layer.options.storage;
            if (storage) {
                storage.add(this._storageKey, this._layer._imageToDataUri(this));
            }
            L.TileLayer.prototype._tileOnLoad.apply(this, arguments);
        },

        _setUpTile: function (tile, key, value, cache) {
            tile._layer = this;
            if (cache) {
                tile._storageKey = key;
                tile.onload = this._tileOnLoadWithCache;
                tile.crossOrigin = 'Anonymous';
            } else {
                tile.onload = this._tileOnLoad;
            }
            tile.onerror = this._tileOnError;
            tile.src = value;
        },

        _loadTile: function (tile, tilePoint) {
            this._adjustTilePoint(tilePoint);
            var key = tilePoint.z + ',' + tilePoint.y + ',' + tilePoint.x;

            var self = this;
            if (this.options.storage) {
                this.options.storage.get(key, function (value) {
                    if (value) {
                        self._setUpTile(tile, key, value, false);
                    } else {
                        self._setUpTile(tile, key, self.getTileUrl(tilePoint), true);
                    }
                }, function () {
                    self._setUpTile(tile, key, self.getTileUrl(tilePoint), true);
                });
            } else {
                self._setUpTile(tile, key, self.getTileUrl(tilePoint), false);
            }
        }
    });

    emr.on('mapLoad', function (storage) {

            mapa_localidad_referencia = L.map('mapa_localidades',{ zoomControl:false });
            mapa_localidad_referencia.dragging.disable();
            mapa_localidad_referencia.touchZoom.disable();
            mapa_localidad_referencia.doubleClickZoom.disable();
            mapa_localidad_referencia.scrollWheelZoom.disable();

            // Disable tap handler, if present.
            if (mapa_localidad_referencia.tap) mapa_localidad_referencia.tap.disable();                         

            mapa_localidad_referencia.setView([19.4614619, -88.300083], 6);


            layer_street = new StorageTileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}', {
                storage: storage,
                maxZoom: 9,
                //id: 'examples.map-i875mjb7',
                //detectRetina: true
            });            

            layer_street2 = new StorageTileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}', {
                storage: storage,
                maxZoom: 11,
                //id: 'examples.map-i875mjb7',
                //detectRetina: true
            });            

            /************* mapa a nivel urbano **********************/
            mapa_manzana_puntos = L.map('mapa_manzana',{ zoomControl:true });
            mapa_manzana_puntos.setView([19.4614619, -88.300083], 6);

            new StorageTileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}', {
                storage: storage,
                maxZoom: 18,
                //id: 'examples.map-i875mjb7',
                //detectRetina: true
            }).addTo(mapa_manzana_puntos);

                    var geojsonLayer = L.geoJson(limites_estatales_municipales, {        
                        style: {
                            "clickable": false,
                            "color": "#AAAAAA",
                            "weight": 5.0,
                            "opacity": 0.9,
                            "strokeDashstyle": "dash",
                        }
                    }).addTo(mapa_manzana_puntos);  


            mapa_manzana_puntos.on('moveend', function () {
                   
                    if (mapa_manzana_puntos.getZoom() < 15 && mapa_manzana_puntos.hasLayer(heatmapLayer)==false) {
                            mapa_manzana_puntos.addLayer(heatmapLayer);
                    }
                    if (mapa_manzana_puntos.getZoom() >= 15 && mapa_manzana_puntos.hasLayer(heatmapLayer)) {
                            mapa_manzana_puntos.removeLayer(heatmapLayer);
                    }

                    if (mapa_manzana_puntos.getZoom() >= 15 && mapa_manzana_puntos.hasLayer(puntos_patentes)==false) {
                            mapa_manzana_puntos.addLayer(puntos_patentes);
                    }
                    if (mapa_manzana_puntos.getZoom() < 15 && mapa_manzana_puntos.hasLayer(puntos_patentes)) {
                            mapa_manzana_puntos.removeLayer(puntos_patentes);
                    }

                    if (mapa_manzana_puntos.getZoom() >= 18 && mapa_manzana_puntos.hasLayer(layer_satelite)) {
                            mapa_manzana_puntos.removeLayer(layer_satelite);
                    }
                    else if(layer_satelite_encendido == true && mapa_manzana_puntos.hasLayer(layer_satelite)==false) {
                            mapa_manzana_puntos.addLayer(layer_satelite);
                    }

                    if (mapa_manzana_puntos.getZoom() >= 15 && mapa_manzana_puntos.hasLayer(puntos_patentes)) {
                            pinta_capa_patentes();
                    }

            });

            /*mapa_manzana_puntos.on('move', function () {
                    pinta_capa_patentes();
            });*/



            var cfg = {
              // radius should be small ONLY if scaleRadius is true (or small radius is intended)
              "radius": 9,
              "maxOpacity": .8, 
              // scales the radius based on map zoom
              "scaleRadius": false, 
              // if set to false the heatmap uses the global maximum for colorization
              // if activated: uses the data maximum within the current map boundaries 
              //   (there will always be a red spot with useLocalExtremas true)
              "useLocalExtrema": true,
              // which field name in your data represents the latitude - default "lat"
              latField: 'latitud',
              // which field name in your data represents the longitude - default "lng"
              lngField: 'longitud',
              // which field name in your data represents the data value - default "value"
              valueField: 'total'
            };

            puntos_patentes = new L.FeatureGroup();
            heatmapLayer = new HeatmapOverlay(cfg);
            
            //map_object.addLayer(puntos_patentes);
            mapa_manzana_puntos.addLayer(heatmapLayer);

        //emr.fire('mapLoaded');
    });

    emr.on('mapLoad2', function (storage2) {

        /*  var Esri_WorldStreetMap = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}', {
                attribution: 'Tiles &copy; Esri &mdash; Source: Esri, DeLorme, NAVTEQ, USGS, Intermap, iPC, NRCAN, Esri Japan, METI, Esri China (Hong Kong), Esri (Thailand), TomTom, 2012'
            });
        */
            /*layer_satelite = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
                attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
            });*/

            //mapa_manzana_puntos.addLayer(layer_satelite);

            layer_satelite = new StorageTileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
                storage: storage2,
                maxZoom: 17
            });

            //emr.fire('mapLoaded2');
    });



})(window, window.offlineMaps.eventManager, L);