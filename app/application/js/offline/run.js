'use strict';

(function (emr) {
    emr.on('storageLoaded', 'mapLoad');
    emr.fire('storageLoad');
    emr.on('storageLoaded2', 'mapLoad2');
    emr.fire('storageLoad2');
})(window.offlineMaps.eventManager);