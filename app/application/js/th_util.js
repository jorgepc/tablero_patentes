/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/* VARIABLES GLOBALES*/

var db;
var ws_indicadores_disponibles     = 'http://148.235.173.216:9080/patjson/consultas/indicadores/.json';
var ws_lista_patentes              = 'http://148.235.173.216:9080/patjson/consultas/patentes/.json';
var ws_datos_resumen_indicadores   = 'http://148.235.173.216:9080/patjson/consultas/datos/.json?id='; //se pasa el id del indicador
var ws_datos_registra_actualizados = 'http://148.235.173.216:9080/patjson/consultas/status_patente/.json?status=0&patentes='; //se pasa el id del indicador
var conectividad = 1;
var version = '1.0.0';
var uuid = '';
var act_tabla_lista_ind = false;
var act_tabla_datos_mun = false;
var act_tabla_datos_loc = false;
var municipios_extent_array = new Array();

var municipios_array = ["Tulum", "Bacalar", "Benito Juárez", "Cozumel", "Isla Mujeres", "José María Morelos", "Lázaro Cárdenas", "Othón P. Blanco", "Solidaridad", "Felipe Carrillo Puerto", "QUINTANA ROO"];
var meses_array = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre","Diciembre"];

var layer_street;
var layer_street2;
var layer_satelite;

var clave_indicador_actual = 1;
var clave_municipio_actual = '';
var clave_localidad_actual = '';
var titulo_indicador_actual = '';
var localidad_actual = '';
var id_grafico_municipio_actual = -1;
var id_grafico_localidad_actual = -1;

var vista_actual = 'municipio';

var lista_id_valor_graficos_municipio = new Array();
var lista_nombres_municipio_valor_grafico = new Array();

var lista_id_valor_graficos_localidad = new Array();

var colores = [];
colores.push("0,0,255"); //Estado darkblue
colores.push("0,147,0"); //Municipio1 green
colores.push("255,128,64"); //Municipio2 orange
colores.push("255,0,255"); //Municipio3 purple
colores.push("128,0,0"); //Municipio4 darkred
colores.push("50,117,158"); //Municipio5
colores.push("0,255,255"); //Municipio6
colores.push("255,128,0"); //Municipio7
colores.push("128,0,255"); //Municipio8
colores.push("255,128,255"); //Municipio9
colores.push("255,255,0"); //Municipio10

var colores_markers = [];
colores_markers.push("darkblue");
colores_markers.push("green");
colores_markers.push("orange");
colores_markers.push("purple");
colores_markers.push("darkred");

var tipo_markers = new Array();
var campo_tabla_marker = '';

var mapa_localidad_referencia;
var mapa_manzana_puntos;
var layer_satelite_encendido = false;
var puntos_graduados;
var puntos_patentes;
var heatmapLayer;
var etiquetas_localidad_visible;
var datos_puntos_patentes_array = new Array();

var legend = L.control({position: 'bottomright'});
var ver_legenda_mapa_manzanas = false;
var legenda_mapa_manzanas_agregada = false;

var html_cuadro_simbologia_manzana = '';

var campos_indicadores_patentes = new  Array();


/*
Obtener solo patentes con cambios
http://162.212.133.207/patentes/consultas/patentes/.json?actualizadas=1

Actualizar el Status de la patentes (Cuando ya las actualizaste en la app debes de enviar el id de las patetes separadas por comas y  el status=0)
http://162.212.133.207/patentes/consultas/status_pantente/.json?status=0&patentes=1630,6814,949

Para fines prácticos si envias status=1 se vuelven a poner para que  al consultr solo patentes actualizadas se cambie de nuevo al bandera a TRUE

http://162.212.133.207/patentes/consultas/status_pantente/.json?status=1&patentes=6361,6571,7951&uuid=Hs223234

http://162.212.133.207/patentes/consultas/patentes/.json?actualizadas=1&uuid=Hs223234
*/


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//document.addEventListener("deviceready", inicia_datos_app, false);
document.addEventListener("load", inicia_datos_app(), false);

function inicia_datos_app()
{
	//Primero inicializamos la base de datos
    db = window.openDatabase("patentesdb", "1.0", "Datos para el dispositivo del tablero de pantentes", 100 * 1024 * 512); //100 mb en bytes

	detecta_uuid()
	
	$.getJSON("municipios_extent.json")  	
		.done(function(data) {

				municipios_extent_array = new Array();

				$.each(data.rows, function(index, rec)
				{
						municipios_extent_array[rec.clave_municipio+'_xmin'] = rec.xmin;
						municipios_extent_array[rec.clave_municipio+'_ymin'] = rec.ymin;
						municipios_extent_array[rec.clave_municipio+'_xmax'] = rec.xmax;
						municipios_extent_array[rec.clave_municipio+'_ymax'] = rec.ymax;
				});
			
		})
		.fail(function() { console.log("Error al cargar el archivo municipios_extent.json")});


		$('#box_2_content').on('hide', function (event) {
				
				$('#box_2_expand').removeClass('icon-minus-sign').addClass('icon-plus-sign');
				
				$("#mapa_manzana").css('height', '685px' );
				event.stopPropagation();
	
		});

		$('#box_2_content').on('show', function (event) {
		
				$('#box_2_expand').removeClass('icon-plus-sign').addClass('icon-minus-sign');
			
				$("#mapa_manzana").css('height', '400px' );
				event.stopPropagation();

		});

		$('#VentanaUtilerias').on('shown', function(){ 

				$("#uuid_txt").val(uuid);
			  	db.transaction(function (tx) {
			        tx.executeSql("SELECT * FROM datos_dispositivo", [], function (tx, results) 
			        {
							$("#descrip_dispositivo").val(results.rows.item(0).descripcion);
							if(results.rows.item(0).validado == 1) {
								$("#txt_estatus_dispositivo").html('<i class="icon-ok"></i> uuid v&aacute;lido');
							}
							else {
								$("#txt_estatus_dispositivo").html('<i class="icon-remove"></i> uuid NO v&aacute;lido');
							}

			        }, function (tx) {
			            console.log('la tabla datos_dispositivo no existe, recrear la base de datos');
			        });
				});


			  	db.transaction(function (tx) {
			        tx.executeSql("SELECT * FROM bitacora_tablas", [], function (tx, results)
			        {
							if(results.rows.length > 0)
							{
								$('#tabla_tablas').empty();
								for (var i= 0; i < results.rows.length; i = i + 1)
								{
									$('#tabla_tablas').append('<tr><td>'+results.rows.item(i).tabla+'</td><td>'+number_format( results.rows.item(i).registros, 0, '.', ',')+'</td></tr>');
								}

							}

			        }, function (tx) {
			            console.log('la tabla bitacora_tablas no existe, recrear la base de datos');
			        });
				});

		});



      var temporizador_conexion = setInterval(function() {

				checa_cambios_datos(function(actualizar_frontend) {
						if(actualizar_frontend == 1)
						{
							actualiza_datos_bd();
						}
						else if(actualizar_frontend == -1)
						{
							actualiza_menu_indicadores();
							cambia_indicador_menu(clave_indicador_actual);						
						    $("#txt_actualizacion").html("Dispositivo no validado");
						    $("#icon_act_loading").html('');
						}
				});
      }, 600000); //Inicialización y ejecución del proceso de revisar cambios en los datos cada 10 min

    //Luego checamos que existan las tablas y tengan datos
    $("#txt_actualizacion").html("Revisando datos locales...");
    $("#icon_act_loading").html('<div class="loading4"><div class="bar"></div><div class="bar"></div><div class="bar"></div><div class="bar"></div></div>');
	checa_exista_tablas(function(existe_bd) {
			if(existe_bd == 0)
			{
				//Sino existen las tablas las creamos
				crea_tablas_datos(function() {
						//Checamos si los datos en la tabla son los ultimos y en dado caso se actualizan desde el servidor
						checa_dispositivo_valido(function(valido) {
							if(valido == 1)
							{
									actualiza_datos_bd();
									muestra_fecha_ultima_actualizacion();								
							}

						});
				});
			}
			else
			{
					//Checamos si los datos en la tabla son los ultimos y en dado caso se actualizan desde el servidor
					checa_cambios_datos(function(actualizar_frontend) {
							if(actualizar_frontend == 1)
							{
								actualiza_datos_bd();
							}
							else if(actualizar_frontend == 0)
							{
								actualiza_menu_indicadores();
								cambia_indicador_menu(clave_indicador_actual);						
								muestra_fecha_ultima_actualizacion();
							}
							else if(actualizar_frontend == -1)
							{
							    $("#txt_actualizacion").html("Dispositivo no validado");
							    $("#icon_act_loading").html('');
							}
					});
			}
	});
}


function checa_dispositivo_valido(callback)
{
	//Primero checamos que el dispositivo sea valido preguntando primero al servidor, sino hay conexion se checa la tabla datos_dispositivo
	$.ajax({
    type: "POST",
    //url: 'http://162.212.133.206/patentes/consultas/indicadores/.json?uuid='+uuid,
    url: ws_indicadores_disponibles+'?uuid='+uuid,
    dataType: "jsonp",
    timeout: 4000,
    error: function(x, t, m)
    {
    	if(t==="timeout") 
    	{
			db.transaction(function(tx) {
			        tx.executeSql('SELECT validado FROM datos_dispositivo', [], function (tx, results)
			        	 {
				    		callback(results.rows.item(0).validado);

			        	 }, function (tx) {
			                
			            });
			});	
    	}
    },
    success: function(data) 
    {
    	var resultado;
    	if(data.codigo > 0) //el dispositivo no esta registrado o validado por lo tanto se borraran todos los datos
    		resultado = 0;
    	else
    		resultado = 1;

			db.transaction(function(tx) {
			        tx.executeSql('UPDATE datos_dispositivo SET validado = ?', [resultado], function (tx, results)
			        	 {
				    		callback(resultado);

			        	 }, function (tx) {
			                
			            });
			});	
    }
	});
}


function checa_cambios_datos(callback)
{
	
	checa_dispositivo_valido(function(valido) {
	
		if(valido == 0)  //si el dispositivo no es valido, solamente regresa el valor -1 y no actualiza la bd	
		{
			/*db.transaction(function (tx) { 
			   tx.executeSql("DELETE FROM bitacora_tablas", []);
			});
			db.transaction(function (tx) { 
			   tx.executeSql("DELETE FROM datos_localidad_indicadores", []);
			});
			db.transaction(function (tx) { 
			   tx.executeSql("DELETE FROM datos_municipio_indicadores", []);
			});
			db.transaction(function (tx) { 
			   tx.executeSql("DELETE FROM indicadores", []);
			});
			db.transaction(function (tx) { 
			   tx.executeSql("DELETE FROM patentes", []);
			});*/
			callback(-1);
		}
		else
		{
				//Si han pasado mas de dos horas, checar si hay actualizaciones en el servidor
				db.transaction( function(tx) {
					        tx.executeSql("SELECT * FROM bitacora_actualizacion", [], function (tx, results)
					        {
					        	if(results.rows.length > 0)
					        	{

									var fecha_ultima = new Date(results.rows.item(0).anio, results.rows.item(0).mes - 1, results.rows.item(0).dia, results.rows.item(0).hora, results.rows.item(0).min);
									var fecha_actual = new Date();
									var minutos = Math.floor( ( fecha_actual.getTime() - fecha_ultima.getTime() ) / (1000 * 60))

									if( minutos >= 120 )
									{
											$("#txt_actualizacion").html("Buscando actualizaciones en el servidor...");
											$("#icon_act_loading").html('<div class="loading4"><div class="bar"></div><div class="bar"></div><div class="bar"></div><div class="bar"></div></div>');

											db.transaction( function(tx) {
												        tx.executeSql('SELECT count(id) as total FROM patentes', [], function (tx, results)
												        {
												        	if(results.rows.item(0).total > 0)
												        	{
																	//checar si hay cambios en la tabla de patentes
																	$.ajax({
																		type: "POST",
																		url: ws_lista_patentes+'?actualizadas=1&uuid='+uuid,
																		dataType: "jsonp",
																	    timeout: 9000,
																	    error: function(x, t, m)
																	    {
																	    	if(t==="timeout") 
																	    	{
																				$("#icon_act_loading").html('');
																				callback(0);	
																	    	}
																	    },
																		success: function(data) 
																		{
																   			$("#icon_act_loading").html('');
																   			if(data.patentes.length > 0)
																   				callback(1);	//si hay cambios y es valido el dispositivo regresar 1
																   			else
																   				callback(0);	//si no hay cambios y es valido el dispositivo regresar 0

																		}//Sucess
																	}); //Ajax
												        	}
												        	else
												        	{
																	callback(1);
												        	}



												       	}, function (tx) {
												                
												        });
											});


									}
									else
									{
										callback(0);
									}

					        	}

					       	}, function (tx) {
					                
					        });
				});


		}

	});
	
}


function guarda_descripcion_dispositivo()
{
		db.transaction(function(tx) {
		        tx.executeSql('UPDATE datos_dispositivo SET descripcion = ?', [$("#descrip_dispositivo").val()], function (tx, results)
		        	 {
		        	 	
		        	 }, function (tx) {
		                console.log('Error al actualizar la tabla datos_dispositivo');
		            });
		});	
}


function checa_exista_tablas(callback)
{
	        db.transaction(
	            function(tx) {
	                tx.executeSql("SELECT name FROM sqlite_master WHERE type='table' AND name='version_app'", this.txErrorHandler,
	                    function(tx, results) {
	                        if (results.rows.length == 1)
								callback(1);
	                        else
								callback(0);
	                    });
	            }
	        );
}


function muestra_fecha_ultima_actualizacion()
{
		db.transaction( function(tx) {
			        tx.executeSql("SELECT * FROM bitacora_actualizacion", [], function (tx, results)
			        {
			        	if(results.rows.length > 0)
			        	{
			        		if(results.rows.item(0).anio > 2000)
			        		{
								var ampm = (results.rows.item(0).hora >= 12) ? " P.M.":" A.M.";
								var Hours = ((results.rows.item(0).hora > 12) ? results.rows.item(0).hora - 12 : results.rows.item(0).hora );
								var Minutes=((results.rows.item(0).min < 10) ? ":0" : ":") + results.rows.item(0).min;

								$("#txt_actualizacion").html("&Uacute;ltima actualizaci&oacute;n: " + results.rows.item(0).dia + '/' + meses_array[results.rows.item(0).mes - 1] + '/' + results.rows.item(0).anio+' - '+(Hours+Minutes+" "+ampm));
			        		}
			        		else
								$("#txt_actualizacion").html("La base de datos local esta vac&iacute;a");

							$("#icon_act_loading").html("");
			        	}
			        	else
			        	{
							$("#txt_actualizacion").html("");
							$("#icon_act_loading").html("");
			        	}

			       	}, function (tx) {

			        });
		});
}



function actualiza_tablas_principales()
{
	if(act_tabla_lista_ind == true && act_tabla_datos_mun == true && act_tabla_datos_loc == true)
	{
		$("#txt_actualizacion").html("Actualizando tablas principales...");
		act_tabla_lista_ind = false;
		act_tabla_datos_mun = false; 
		act_tabla_datos_loc = false;

			db.transaction(function (tx) {
			        tx.executeSql('DELETE FROM indicadores', [], function (tx, results)
			        	 {
								db.transaction(function (tx) {
								        tx.executeSql('INSERT INTO indicadores (indicadore_id, nombre, descripcion, formula, campo) SELECT * FROM indicadores_temp ORDER BY indicadore_id ASC', [], function (tx, results)
								        	 {
								        	 			//Actualizacion de la tabla de datos a nivel municipio principal
														db.transaction(function (tx) {
														        tx.executeSql('DELETE FROM datos_municipio_indicadores', [], function (tx, results)
														        	 {
																			db.transaction(function (tx) {
																			        tx.executeSql('INSERT INTO datos_municipio_indicadores (indicadore_id, clave_municipio, municipio, elemento, valor, porcentaje) SELECT * FROM datos_municipio_indicadores_temp ORDER BY indicadore_id ASC, clave_municipio ASC', [], function (tx, results)
																			        	 {

																		        	 			//Actualizacion de la tabla de datos a nivel localidad principal
																								db.transaction(function (tx) {
																								        tx.executeSql('DELETE FROM datos_localidad_indicadores', [], function (tx, results)
																								        	 {
																													db.transaction(function (tx) {
																													        tx.executeSql('INSERT INTO datos_localidad_indicadores (indicadore_id, clave_municipio, clave_localidad, localidad, elemento, valor, porcentaje) SELECT * FROM datos_localidad_indicadores_temp ORDER BY indicadore_id ASC, clave_municipio ASC, clave_localidad ASC', [], function (tx, results)
																													        	 {

																																			db.transaction(function (tx) { 
																																			   tx.executeSql("DELETE FROM indicadores_temp", []);
																																			});

																																			db.transaction(function (tx) { 
																																			   tx.executeSql("DELETE FROM datos_municipio_indicadores_temp", []);
																																			});

																																			db.transaction(function (tx) { 
																																			   tx.executeSql("DELETE FROM datos_localidad_indicadores_temp", []);
																																			});

																																			registra_bitacora_tablas('indicadores');
																																			registra_bitacora_tablas('datos_municipio_indicadores');
																																			registra_bitacora_tablas('datos_localidad_indicadores');


																																		    $("#txt_actualizacion").html("Descargando lista de patentes...");
																																		    db.transaction( function(tx) {
																																				        tx.executeSql('SELECT count(id) as total FROM patentes', [], function (tx, results)
																																				        {
																																				        	if(results.rows.item(0).total > 0)
																																				        	{
																																				        		var urlpatentes = ws_lista_patentes+'?actualizadas=1&uuid='+uuid;
																																				        		var sql = 'UPDATE patentes SET numero_licencia = ?, tipo_establecimiento = ?, giro = ?, giro_principal = ?, nombre_comercial_licencia = ?, manzana_domicilio = ?, smza_domicilio = ?, lote = ?, domicilio_establecimiento = ?, '+
																																										  'no_int_dom_establecimiento = ?, no_ext_dom_establecimiento = ?, codigo_postal_establecimiento = ?, colonia_establecimiento = ?, clave_municipio = ?, municipio = ?, clave_localidad = ?, localidad = ?, observaciones = ?, '+
																																										  'latitud = ?, longitud = ?, adeudo = ?, comodato = ?, monto_adeudo = ?, costo_patente = ? WHERE id = ?';
																																								var tipo_actualizacion = 'update';
																																								var mensaje = 'Actualizando cambios en la lista de patentes: ';
																																				        	}
																																				        	else
																																				        	{
																																				        		var urlpatentes = ws_lista_patentes+'?uuid='+uuid;
																																				        		var sql = 'INSERT INTO patentes (numero_licencia, tipo_establecimiento, giro, giro_principal, nombre_comercial_licencia, manzana_domicilio, smza_domicilio, lote, domicilio_establecimiento, '+
																																										  'no_int_dom_establecimiento, no_ext_dom_establecimiento, codigo_postal_establecimiento, colonia_establecimiento, clave_municipio, municipio, clave_localidad, localidad, observaciones, latitud, longitud, adeudo, comodato, monto_adeudo, costo_patente, id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
																																								var tipo_actualizacion = 'insert';
																																								var mensaje = 'Actualizando lista de patentes completa: ';
																																				        	}

																																							//Actualiza la tabla de patentes
																																							$.ajax({
																																								type: "POST",
																																								url: urlpatentes,
																																								dataType: "jsonp",
																																							    error: function(x, t, m)
																																							    {
																																							    	console.log(t);
																																							    },
																																								success: function(data) 
																																								{
																																						   				var total = data.patentes.length;
																																						   				var num_limite = parseInt(total / 5);
																																						   				var contador = 0;
																																						   				var cadena_ids = '';
																																							    		$("#txt_actualizacion").html("Actualizando lista de patentes...");
																																							    		$("#icon_act_loading").html('<div class="loading4"><div class="bar"></div><div class="bar"></div><div class="bar"></div><div class="bar"></div></div>');

																																													if(tipo_actualizacion == 'insert')
																																													{
																																															db.transaction(function (tx) { 
																																															   tx.executeSql("DELETE FROM patentes", []);
																																															});												
																																													}

																																										      		$.each(data.patentes, function(index, rec)
																																										      		{
																																										      				var registro = rec.Patente;
																																															
																																															if(tipo_actualizacion == 'update')
																																															{
																																																	db.transaction( function(tx) {
																																																		        tx.executeSql("SELECT id FROM patentes WHERE id = ?", [registro.id], function (tx, results)
																																																		        {
																																																		        	if(results.rows.length == 0)
																																																		        	{
																																																			        		sql = 'INSERT INTO patentes (numero_licencia, tipo_establecimiento, giro, giro_principal, nombre_comercial_licencia, manzana_domicilio, smza_domicilio, lote, domicilio_establecimiento, '+
																																																									  'no_int_dom_establecimiento, no_ext_dom_establecimiento, codigo_postal_establecimiento, colonia_establecimiento, clave_municipio, municipio, clave_localidad, localidad, observaciones, latitud, longitud, adeudo, comodato, monto_adeudo, costo_patente, id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';

																																																		        	}
																																																		       	}, function (tx) {
																																																		                
																																																		        });
																																																	});

																																															}

																																															db.transaction(function(tx) {
																																															        tx.executeSql(sql, 
																																															        	[registro.numero_licencia, registro.tipo_establecimiento, registro.giro, registro.giro_principal, registro.nombre_comercial_licencia, registro.manzana_domicilio, registro.smza_domicilio, registro.lote, registro.domicilio_establecimiento,
																																															        	 registro.no_int_dom_establecimiento, registro.no_ext_dom_establecimiento, registro.codigo_postal_establecimiento, registro.colonia_establecimiento, registro.clave_municipio, registro.municipio, registro.clave_localidad, registro.localidad, registro.observaciones, registro.latitud, registro.longitud, 
																																															        	 registro.adeudo, registro.comodato, registro.monto_adeudo, registro.costo_patente, registro.id], function (tx, results)
																																															        	 {
																																															        	 	cadena_ids += registro.id + ',';
																																															        	 	contador++;

																																															        	 	if(contador == num_limite)
																																															        	 	{
																																															        	 		var ids_final =  cadena_ids.substring(0, cadena_ids.length - 1);
																																															        	 		//console.log(ids_final);

																																																				cadena_ids = '';
																																																				contador = 0;
																																																				
																																																				$.ajax({
																																																					type: "POST",
																																																					url: ws_datos_registra_actualizados + ids_final + '&uuid='+uuid,
																																																					dataType: "jsonp",
																																																				    error: function(x, t, m)
																																																				    {
																																																				    	console.log(t);
																																																				    },
																																																					success: function(data) 
																																																					{
																																																					}
																																																				});
																																															        	 	}

																																															        	 	$("#txt_actualizacion").html(mensaje + parseInt( ( ((index+1) / total) * 100) ) + "%");
																																															        	 }, function (tx) {
																																															                console.log('Error en la carga de datos en la tabla de patentes');
																																															            });
																																															});
																																										      		});
																																													

																																													if(cadena_ids.length > 0)
																																													{
																																										        	 		var ids_final =  cadena_ids.substring(0, cadena_ids.length - 1);
																																										        	 		//console.log(ids_final);

																																															$.ajax({
																																																type: "POST",
																																																url: ws_datos_registra_actualizados + ids_final + '&uuid='+uuid,
																																																dataType: "jsonp",
																																															    error: function(x, t, m)
																																															    {
																																															    	console.log(t);
																																															    },
																																																success: function(data) 
																																																{
																																																	cadena_ids = '';
																																																	contador = 0;
																																																}
																																															});																																														
																																													}


																																													if(tipo_actualizacion == 'insert')
																																													{

																																														db.transaction(
																																														  function(tx) {
																																														      tx.executeSql("CREATE UNIQUE INDEX 'idxpatentes_id' ON 'patentes' ('id' ASC); "+
																																														      				"CREATE INDEX 'idxpatentes_municipio' ON 'patentes' ('clave_municipio' ASC);"+
																																														      				"CREATE INDEX 'idxpatentes_localidad' ON 'patentes' ('clave_localidad' ASC);", this.txErrorHandler,
																																														          function(tx, results) {
																																														          });
																																														  }
																																														);
																																													}

																																													registra_bitacora_tablas('patentes');
																																													var d = new Date();
																																													dia = d.getDate();
																																													mes = d.getMonth()+1;
																																													anio = d.getFullYear();
																																													hora = d.getHours();
																																													min = d.getMinutes();

																																													db.transaction(function (tx) { 
																																													   tx.executeSql("UPDATE bitacora_actualizacion SET dia = ?, mes = ?, anio = ?, hora = ?, min = ?", [dia, mes, anio, hora, min]);
																																													});

																																													actualiza_menu_indicadores();
																																													cambia_indicador_menu(clave_indicador_actual);
																																													muestra_fecha_ultima_actualizacion();

																																								}//Sucess
																																							}); //Ajax


																																				       	}, function (tx) {
																																				                
																																				        });
																																		    });


																													        	 }, function (tx) {
																													                console.log('Error 6. INSERT INTO datos_localidad_indicadores (indicadore_id, clave_municipio, clave_localidad, localidad, elemento, valor, porcentaje) SELECT * FROM datos_localidad_indicadores_temp ORDER BY indicadore_id ASC, clave_municipio ASC, clave_localidad ASC');
																													            });
																													});

																								        	 }, function (tx) {
																								                console.log('Error 5');
																								            });
																								});

																			        	 }, function (tx) {
																			                console.log('Error 4');
																			            });
																			});

														        	 }, function (tx) {
														                console.log('Error 3');
														            });
														});


								        	 }, function (tx) {
								                console.log('Error 2');
								            });
								});

			        	 }, function (tx) {
			                console.log('Error 1');
			            });
			});

	}//end if

}


function actualiza_datos_bd()
{
	$("#txt_actualizacion").html("Descargando lista de indicadores...");
	$("#icon_act_loading").html('<div class="loading4"><div class="bar"></div><div class="bar"></div><div class="bar"></div><div class="bar"></div></div>');

	db.transaction(function (tx) { 
	   tx.executeSql("DELETE FROM indicadores_temp", []);
	});

	db.transaction(function (tx) { 
	   tx.executeSql("DELETE FROM datos_municipio_indicadores_temp", []);
	});

	db.transaction(function (tx) { 
	   tx.executeSql("DELETE FROM datos_localidad_indicadores_temp", []);
	});

	//Actualiza la tabla temporal de indicadores
	$.ajax({
	    type: "POST",
	    url: ws_indicadores_disponibles+'?uuid='+uuid,
	    dataType: "jsonp",
        timeout: 6000,
	    error: function(x, t, m)
	    {
           if(t==="timeout")
           {
              alert("No se pudo conectar con el servidor central");
              muestra_fecha_ultima_actualizacion();
           }
	    },
	    success: function(data) 
	    {
				if(data.indicadores.length > 0)
				{
							var cont = 0;
							$.each(data.indicadores, function(index, rec)
							{
								  var registro = rec.Indicadore;    
								  var registro2 = rec.Xml.fields;
								  if(registro2.titulo == 'titulo')
								  	var valor =  '';
								  else
								  	var valor = registro2.titulo;

									db.transaction(function (tx) {
									        tx.executeSql('INSERT INTO indicadores_temp (indicadore_id, nombre, descripcion, formula, campo) VALUES (?,?,?,?,?)', 
									        	[registro.indicadore_id, registro.nombre, registro.descripcion, registro.formula, valor ], function (tx, results)
									        	{
									        		cont++;
									        	}, function (tx) {
									                console.log('Error en la sentencia. INSERT INTO indicadores_temp (indicadore_id, nombre, descripcion, formula, campo) VALUES (?,?,?,?,?)');
									            });
									});
							});

			        		act_tabla_lista_ind = true;
			        		actualiza_tablas_principales();					


						    //Actualiza las tablas a nivel municipio y localidad de los datos de los indicadores

							$("#txt_actualizacion").html("Descargando actualizaciones de los indicadores...");
							$("#icon_act_loading").html('<div class="loading4"><div class="bar"></div><div class="bar"></div><div class="bar"></div><div class="bar"></div></div>');

							db.transaction( function(tx) {
								        tx.executeSql('SELECT indicadore_id FROM indicadores_temp ORDER BY indicadore_id', [], function (tx, results)
								        {
								        	var total_indicadores = results.rows.length;
								        	var contador = 0;
								        	for (i= 0; i < results.rows.length; i = i + 1)
								        	{
								        		var indicadore_id = results.rows.item(i).indicadore_id;


														      $.ajax({
														            type: "POST",
														            url: ws_datos_resumen_indicadores + indicadore_id+'&uuid='+uuid,
														            dataType: "jsonp",
																    error: function(x, t, m)
																    {
																    	console.log(t);
																    },
														            success: function(data) 
														            {
																			
																			var total = data.datos.length;
																			var indicadore_id_actual = 0;
																			$.each(data.datos, function(index, registro)
																			{
																				  	if(registro.nivel != 'localidad' && (municipios_array.indexOf(registro.nombre_columna) >= 0) )
																				  	{
																							db.transaction(function(tx) {
																							        tx.executeSql('INSERT INTO datos_municipio_indicadores_temp (indicadore_id, clave_municipio, municipio, elemento, valor, porcentaje) VALUES (?,?,?,?,?,?)', 
																							        	[registro.indicadore_id, registro.clave_municipio, registro.nombre_columna, registro.titulo, registro.valor, 0], function (tx, results)
																							        	 {
																							        	 	
																							        	 }, function (tx) {
																							                console.log('Error en la sentencia: INSERT INTO datos_municipio_indicadores_temp (indicadore_id, clave_municipio, municipio, elemento, valor, porcentaje) VALUES (?,?,?,?,?,?)');
																							            });
																							});

																					}
																				  	else if(registro.nivel == 'localidad')
																				  	{

																							db.transaction(function(tx) {
																							        tx.executeSql('INSERT INTO datos_localidad_indicadores_temp (indicadore_id, clave_municipio, clave_localidad, localidad, elemento, valor, porcentaje) VALUES (?,?,?,?,?,?,?)', 
																							        	[registro.indicadore_id, registro.clave_municipio, registro.clave_localidad, registro.nombre_columna, registro.titulo, registro.valor, 0], function (tx, results)
																							        	 {
																							        	 	if(indicadore_id_actual != registro.indicadore_id)
																							        	 	{
																							        	 		indicadore_id_actual = registro.indicadore_id;
																							        	 		contador++;
																							        	 	}

																							        	 	$("#txt_actualizacion").html("Descargando datos de cada indicador ("+contador+" de "+total_indicadores+"): " + parseInt( ( ((index+1) / total) * 100) ) + "%");
																							        	 }, function (tx) {
																							                console.log('Error en la sentencia: INSERT INTO datos_localidad_indicadores_temp (indicadore_id, clave_municipio, clave_localidad, localidad, elemento, valor, porcentaje) VALUES (?,?,?,?,?,?,?)');
																							                console.log(registro.indicadore_id+"--"+registro.clave_municipio+"--"+registro.clave_localidad+"--"+registro.nombre_columna+"--"+registro.titulo+"--"+registro.valor+"--"+0);
																							            });
																							});

																					}
																			});
																			
																			db.transaction( function(tx) {
																				        tx.executeSql("SELECT indicadore_id, SUM(valor) as total FROM datos_municipio_indicadores_temp WHERE municipio = 'QUINTANA ROO' GROUP BY indicadore_id", [], function (tx, results)
																				        {
																						        db.transaction( function(tx) {
																							        	for (i= 0; i < results.rows.length; i = i + 1)
																							        	{
																							        		if(results.rows.item(i).indicadore_id == 9)
																										        tx.executeSql('UPDATE datos_municipio_indicadores_temp SET porcentaje = valor WHERE indicadore_id = ?', [results.rows.item(i).indicadore_id]);
																										    else
																										        tx.executeSql('UPDATE datos_municipio_indicadores_temp SET porcentaje = ROUND((CAST(valor AS FLOAT) / ?) * 100,1) WHERE indicadore_id = ?', [results.rows.item(i).total, results.rows.item(i).indicadore_id]);

																										}

																						        		act_tabla_datos_mun = true;
																						        		actualiza_tablas_principales();
																						        });

																				       	}, function (tx) {
																				                
																				        });
																			});


																			db.transaction( function(tx) {
																				        tx.executeSql("SELECT indicadore_id, clave_municipio, SUM(valor) as total FROM datos_localidad_indicadores_temp GROUP BY indicadore_id, clave_municipio", [], function (tx, results)
																				        {

																						        db.transaction( function(tx) {
																							        	for (i= 0; i < results.rows.length; i = i + 1)
																							        	{
																							        		//if(results.rows.item(i).indicadore_id == 9)
																										    //    tx.executeSql('UPDATE datos_localidad_indicadores SET porcentaje = valor WHERE indicadore_id = ?', [results.rows.item(i).indicadore_id]);
																										    //else
																										        tx.executeSql('UPDATE datos_localidad_indicadores_temp SET porcentaje = ROUND((CAST(valor AS FLOAT) / ?) * 100,1) WHERE indicadore_id = ? AND clave_municipio = ?', [results.rows.item(i).total, results.rows.item(i).indicadore_id, results.rows.item(i).clave_municipio]);

																										}
																						        		act_tabla_datos_loc = true;
																						        		actualiza_tablas_principales();
																						        });

																				       	}, function (tx) {
																				                
																				        });
																			});


														            }//Sucess
														      }); //Ajax



											}

								       	}, function (tx) {
								                
								        });
							});

				}
				else
				{
							muestra_fecha_ultima_actualizacion();
				}


	    }//Sucess
	}); //Ajax

}




function registra_bitacora_tablas(tabla)
{
        db.transaction( function(tx) {
                tx.executeSql("DELETE FROM bitacora_tablas WHERE tabla = ?", [tabla], function (tx, results)
                {
				        db.transaction( function(tx) {
							        tx.executeSql('SELECT count(*) as total FROM '+tabla, [], function (tx, results)
							        {
										        db.transaction( function(tx) {
													        tx.executeSql('INSERT INTO bitacora_tablas (tabla, registros) VALUES (?,?)', [tabla, results.rows.item(0).total], function (tx, results)
													        {

													       	}, function (tx) {
													                console.log('Error al insertar en la tabla bitacora_tablas');
													        });
										        });
							       	}, function (tx) {
							                console.log('Error registra_bitacora_tablas');
							        });


				        });

                }, function (tx) {
		                console.log('Error al visualizar los datos del actuario');
		        });
        });

}


function crea_tablas_datos(callback)
{
		//Tabla de version de la app
		db.transaction(
		  function(tx) {
		      var sql =
		          "CREATE TABLE IF NOT EXISTS version_app ( " +
		          "version VARCHAR(5))";

		      tx.executeSql(sql, this.txErrorHandler,
		          function(tx, results) {

		              db.transaction(function (tx) { 
		                  tx.executeSql('DELETE FROM version_app', []);
		              });
		              db.transaction(
		                  function(tx) {
		                    tx.executeSql('INSERT INTO version_app (version) VALUES (?)', [version]);
		                  }
		              );
		          });
		  }
		);


		//Tabla de datos del dispositivo
		db.transaction(
		  function(tx) {
		      var sql =
		          "CREATE TABLE IF NOT EXISTS datos_dispositivo ( " +
		          "descripcion VARCHAR(25), " +
		          "validado INTEGER)";

		      tx.executeSql(sql, this.txErrorHandler,
		          function(tx, results) {
						db.transaction(function (tx) { 
						   tx.executeSql("INSERT INTO datos_dispositivo (descripcion,validado) VALUES (?,?)", ['sin descripcion',0]);
						});
		          });
		  }
		);


		//Tabla de bitacora de actualizaciones
		db.transaction(
		  function(tx) {
		      var sql =
		          "CREATE TABLE IF NOT EXISTS bitacora_actualizacion ( " +
		          "dia INTEGER, "+
		          "mes INTEGER, "+
		          "anio INTEGER, "+
		          "hora INTEGER, "+
		          "min INTEGER)";

		      tx.executeSql(sql, this.txErrorHandler,
		          function(tx, results) {
	
						db.transaction(function (tx) { 
						   tx.executeSql("INSERT INTO bitacora_actualizacion (dia, mes, anio, hora, min) VALUES (?,?,?,?,?)", [1, 1, 1900, 0, 1]);
						});

		          });
		  }
		);


		//Tabla de numero de registros por tabla
		db.transaction(
		  function(tx) {
		      var sql =
		          "CREATE TABLE IF NOT EXISTS bitacora_tablas ( " +
		          "tabla VARCHAR(25), " +
		          "registros INTEGER)";

		      tx.executeSql(sql, this.txErrorHandler,
		          function(tx, results) {
		          });
		  }
		);


		//Tabla de la lista y detalles de los indicadores
		db.transaction(
		  function(tx) {
		      var sql =
		          "CREATE TABLE IF NOT EXISTS indicadores ( " +
		          "indicadore_id INTEGER, " +
		          "nombre VARCHAR(250), " +
		          "descripcion TEXT, " +
		          "formula VARCHAR(300), " +
		          "campo VARCHAR(10))";

		      tx.executeSql(sql, this.txErrorHandler,
		          function(tx, results) {
							db.transaction(
							  function(tx) {
							      tx.executeSql("CREATE UNIQUE INDEX 'idxindicadores_indicadore_id' ON 'indicadores' ('indicadore_id' ASC)", this.txErrorHandler,
							          function(tx, results) {
							          });
							  }
							);
		          });
		  }
		);


		//Tabla de la lista y detalles de los indicadores temporal (es el que se actualiza)
		db.transaction(
		  function(tx) {
		      var sql =
		          "CREATE TABLE IF NOT EXISTS indicadores_temp ( " +
		          "indicadore_id INTEGER, " +
		          "nombre VARCHAR(250), " +
		          "descripcion TEXT, " +
		          "formula VARCHAR(300), " +
		          "campo VARCHAR(10))";

		      tx.executeSql(sql, this.txErrorHandler,
		          function(tx, results) {
							db.transaction(
							  function(tx) {
							      tx.executeSql("CREATE UNIQUE INDEX 'idxindicadores_temp_indicadore_id' ON 'indicadores_temp' ('indicadore_id' ASC)", this.txErrorHandler,
							          function(tx, results) {
							          });
							  }
							);
		          });
		  }
		);

	
		//Tabla de pantentes 
		db.transaction(
		  function(tx) {
		      var sql =
					"CREATE TABLE IF NOT EXISTS patentes ( " +
					"id INTEGER, " +
					"numero_licencia VARCHAR(100), " +
					"tipo_establecimiento VARCHAR(150), " +
					"giro VARCHAR(250), " +
					"giro_principal VARCHAR(250), " +
					"nombre_comercial_licencia TEXT, " +
					"manzana_domicilio VARCHAR(50), " +
					"smza_domicilio VARCHAR(50), " +
					"lote VARCHAR(50), " +
					"domicilio_establecimiento TEXT, " +
					"no_int_dom_establecimiento VARCHAR(50), " +
					"no_ext_dom_establecimiento VARCHAR(50), " +
					"codigo_postal_establecimiento INTEGER, " +
					"colonia_establecimiento VARCHAR(250), " +
					"clave_municipio VARCHAR(3), " +
					"municipio VARCHAR(100), " +
					"clave_localidad VARCHAR(4), " +
					"localidad VARCHAR(150), " +
					"observaciones TEXT, " +
					"latitud DOUBLE PRECISION, " +
					"longitud DOUBLE PRECISION, " +
					"adeudo CHAR(1), " +
					"comodato CHAR(1),  " +
					"monto_adeudo DOUBLE PRECISION DEFAULT 0, " +
					"costo_patente DOUBLE PRECISION DEFAULT 0)";

		      tx.executeSql(sql, this.txErrorHandler,
		          function(tx, results) {
		          });
		  }
		);
	

	
		//Tabla de datos resumen de los indicadores a nivel municipio
		db.transaction(
		  function(tx) {
		      var sql =
					"CREATE TABLE IF NOT EXISTS datos_municipio_indicadores ( " +
					"indicadore_id INTEGER, " +
					"clave_municipio VARCHAR(3), " +
					"municipio VARCHAR(65), " +
					"elemento VARCHAR(45), " +
					"valor UNSIGNED BIG INT, " +
					"porcentaje DOUBLE PRECISION)";

		      tx.executeSql(sql, this.txErrorHandler,
		          function(tx, results) {
							db.transaction(
							  function(tx) {
							      tx.executeSql("CREATE INDEX 'idxdatos_municipio_indicadores_indicadore_id' ON 'datos_municipio_indicadores' ('indicadore_id' ASC); "+
							      				"CREATE INDEX 'idxdatos_municipio_indicadores_clave_municipio' ON 'datos_municipio_indicadores' ('clave_municipio' ASC);", this.txErrorHandler,
							          function(tx, results) {
							          });
							  }
							);
		          });
		  }
		);

		//Tabla de datos resumen de los indicadores a nivel municipio temporal (es la que se actualiza)
		db.transaction(
		  function(tx) {
		      var sql =
					"CREATE TABLE IF NOT EXISTS datos_municipio_indicadores_temp ( " +
					"indicadore_id INTEGER, " +
					"clave_municipio VARCHAR(3), " +
					"municipio VARCHAR(65), " +
					"elemento VARCHAR(45), " +
					"valor UNSIGNED BIG INT, " +
					"porcentaje DOUBLE PRECISION)";

		      tx.executeSql(sql, this.txErrorHandler,
		          function(tx, results) {
							db.transaction(
							  function(tx) {
							      tx.executeSql("CREATE INDEX 'idxdatos_municipio_indicadores_temp_indicadore_id' ON 'datos_municipio_indicadores_temp' ('indicadore_id' ASC); "+
							      				"CREATE INDEX 'idxdatos_municipio_indicadores_temp_clave_municipio' ON 'datos_municipio_indicadores_temp' ('clave_municipio' ASC);", this.txErrorHandler,
							          function(tx, results) {
							          });
							  }
							);
		          });
		  }
		);
	
		//Tabla de datos resumen de los indicadores a nivel localidad
		db.transaction(
		  function(tx) {
		      var sql =
					"CREATE TABLE IF NOT EXISTS datos_localidad_indicadores ( " +
					"indicadore_id INTEGER, " +
					"clave_municipio VARCHAR(3), " +
					"clave_localidad VARCHAR(4), " +
					"localidad VARCHAR(95), " +
					"elemento VARCHAR(45), " +
					"valor UNSIGNED BIG INT, " +
					"porcentaje DOUBLE PRECISION)";

		      tx.executeSql(sql, this.txErrorHandler,
		          function(tx, results) {
							db.transaction(
							  function(tx) {
							      tx.executeSql("CREATE INDEX 'idxdatos_localidad_indicadores_indicadore_id' ON 'datos_localidad_indicadores' ('indicadore_id' ASC); "+
							      				"CREATE INDEX 'idxdatos_localidad_indicadores_clave_localidad' ON 'datos_localidad_indicadores' ('clave_municipio' ASC, 'clave_localidad' ASC);", this.txErrorHandler,
							          function(tx, results) {
							          });
							  }
							);
		          });
		  }
		);
	
		//Tabla de datos resumen de los indicadores a nivel localidad temporal (es la que se actualiza)
		db.transaction(
		  function(tx) {
		      var sql =
					"CREATE TABLE IF NOT EXISTS datos_localidad_indicadores_temp ( " +
					"indicadore_id INTEGER, " +
					"clave_municipio VARCHAR(3), " +
					"clave_localidad VARCHAR(4), " +
					"localidad VARCHAR(95), " +
					"elemento VARCHAR(45), " +
					"valor UNSIGNED BIG INT, " +
					"porcentaje DOUBLE PRECISION)";

		      tx.executeSql(sql, this.txErrorHandler,
		          function(tx, results) {
							db.transaction(
							  function(tx) {
							      tx.executeSql("CREATE INDEX 'idxdatos_localidad_indicadores_temp_indicadore_id' ON 'datos_localidad_indicadores_temp' ('indicadore_id' ASC); "+
							      				"CREATE INDEX 'idxdatos_localidad_indicadores_temp_clave_localidad' ON 'datos_localidad_indicadores_temp' ('clave_municipio' ASC, 'clave_localidad' ASC);", this.txErrorHandler,
							          function(tx, results) {
							          });
							  }
							);
		          });
		  }
		);

		//Tabla de localidades y sus coordenadas
		db.transaction(
		  function(tx) {
		      var sql =
		          "CREATE TABLE IF NOT EXISTS layer_localidades ( " +
		          "id INTEGER PRIMARY KEY AUTOINCREMENT, "+
		          "clave_municipio VARCHAR(3), "+
		          "clave_localidad VARCHAR(5), "+
		          "nombre VARCHAR(80), "+
				  "latitud DOUBLE PRECISION, " +
				  "longitud DOUBLE PRECISION)";

		      tx.executeSql(sql, this.txErrorHandler,
		          function(tx, results) {
	
						db.transaction(function (tx) { 
						   tx.executeSql("DELETE FROM layer_localidades", []);
						});


						$.getJSON("layer_localidades.json")
							.done(function(data) {

									$.each(data.rows, function(index, rec)
									{
											db.transaction(function (tx) {
											        tx.executeSql('INSERT INTO layer_localidades (clave_municipio, clave_localidad, nombre, longitud, latitud) VALUES (?,?,?,?,?)', 
											        	[rec.clave_municipio, rec.clave_localidad, rec.nombre, rec.longitud, rec.latitud], function (tx, results)
											        	{
																db.transaction(
																  function(tx) {
																      tx.executeSql("CREATE INDEX 'idxdatos_layer_localidades_clave_munloc' ON 'layer_localidades' ('clave_municipio' ASC, 'clave_localidad' ASC);", this.txErrorHandler,
																          function(tx, results) {
																          });
																  }
																);

											        	}, function (tx) {
											                console.log('Error en la sentencia: INSERT INTO layer_localidades (clave_municipio, clave_localidad, nombre, longitud, latitud) VALUES (?,?,?,?,?)');
											            });
											});
									});
								
							})
							.fail(function() { console.log("Error al cargar el archivo layer_localidades.json")});

		          });
		  }
		);


		//Tabla de manchas urbanas y su extension
		db.transaction(
		  function(tx) {
		      var sql =
		          "CREATE TABLE IF NOT EXISTS layer_ciudades_extent ( " +
		          "id INTEGER PRIMARY KEY AUTOINCREMENT, "+
		          "clave_municipio VARCHAR(3), "+
		          "clave_localidad VARCHAR(5), "+
		          "nombre_localidad VARCHAR(100), "+
				  "xmin DOUBLE PRECISION, " +
				  "ymin DOUBLE PRECISION, " +
				  "xmax DOUBLE PRECISION, " +
				  "ymax DOUBLE PRECISION)";

		      tx.executeSql(sql, this.txErrorHandler,
		          function(tx, results) {
	
						db.transaction(function (tx) { 
						   tx.executeSql("DELETE FROM layer_ciudades_extent", []);
						});


						$.getJSON("manchas_urbanas_extent.json")  	
							.done(function(data) {

									$.each(data.rows, function(index, rec)
									{
											db.transaction(function (tx) {
											        tx.executeSql('INSERT INTO layer_ciudades_extent (clave_municipio, clave_localidad, nombre_localidad, xmin, ymin, xmax, ymax) VALUES (?,?,?,?,?,?,?)', 
											        	[rec.clave_municipio, rec.clave_localidad, rec.nombre_localidad, rec.xmin, rec.ymin, rec.xmax, rec.ymax], function (tx, results)
											        	{

																db.transaction(
																  function(tx) {
																      tx.executeSql("CREATE INDEX 'idxdatos_layer_ciudades_extent_clave_munloc' ON 'layer_ciudades_extent' ('clave_municipio' ASC, 'clave_localidad' ASC);", this.txErrorHandler,
																          function(tx, results) {
																          });
																  }
																);

											        	}, function (tx) {
											                console.log('Error en la sentencia: INSERT INTO layer_ciudades_extent (clave_municipio, clave_localidad, nombre_localidad, xmin, ymin, xmax, ymax) VALUES (?,?,?,?,?,?,?)');
											            });
											});
									});
								
							})
							.fail(function() { console.log("Error al cargar el archivo manchas_urbanas_extent.json")});

		          });
		  }
		);
	
	callback();
}



function borra_tablas()
{
    var r = confirm("¿Est\u00e1 seguro de borrar todos los datos?");
    if (r == true) {

			db.transaction(function (tx) { 
			   tx.executeSql("DROP TABLE version_app", []);
			});	
			db.transaction(function (tx) { 
			   tx.executeSql("DROP TABLE bitacora_actualizacion", []);
			});
			db.transaction(function (tx) { 
			   tx.executeSql("DROP TABLE bitacora_tablas", []);
			});
			db.transaction(function (tx) { 
			   tx.executeSql("DROP TABLE datos_dispositivo", []);
			});
			db.transaction(function (tx) { 
			   tx.executeSql("DROP TABLE datos_localidad_indicadores", []);
			});
			db.transaction(function (tx) { 
			   tx.executeSql("DROP TABLE datos_localidad_indicadores_temp", []);
			});
			db.transaction(function (tx) { 
			   tx.executeSql("DROP TABLE datos_municipio_indicadores", []);
			});
			db.transaction(function (tx) { 
			   tx.executeSql("DROP TABLE datos_municipio_indicadores_temp", []);
			});
			db.transaction(function (tx) { 
			   tx.executeSql("DROP TABLE indicadores", []);
			});
			db.transaction(function (tx) { 
			   tx.executeSql("DROP TABLE indicadores_temp", []);
			});
			db.transaction(function (tx) { 
			   tx.executeSql("DROP TABLE layer_ciudades_extent", []);
			});
			db.transaction(function (tx) { 
			   tx.executeSql("DROP TABLE layer_localidades", []);
			});
			db.transaction(function (tx) { 
			   tx.executeSql("DROP TABLE patentes", []);
			});

				crea_tablas_datos(function() {
					
					$("#txt_actualizacion").html("La base de datos local esta vac&iacute;a");
    				$("#icon_act_loading").html('');

					actualiza_menu_indicadores();
					cambia_indicador_menu(clave_indicador_actual);						

				    r = confirm("¿Desea recargar los datos desde el servidor central?");
				    if (r == true) {
				    	actualiza_datos_bd();
				    }

				});
			$('#VentanaUtilerias').modal('hide');

    }
 
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function detecta_uuid()
{
	//uuid = device.uuid;
	uuid = '172583EC-40A3-4ABA-A881-73AD6AC8990C';	
}


function checarconexion(callback)
{
	
	  callback(1);

/*      var networkState = navigator.network.connection.type;
      var states = {};
      states[Connection.UNKNOWN]  = 'Unknown connection';
      states[Connection.ETHERNET] = 'Ethernet connection';
      states[Connection.WIFI]     = 'WiFi connection';
      states[Connection.CELL_2G]  = 'Cell 2G connection';
      states[Connection.CELL_3G]  = 'Cell 3G connection';
      states[Connection.CELL_4G]  = 'Cell 4G connection';
      states[Connection.NONE]     = 'sin_conexion';
      
      if(networkState == 'none')
      {
        callback(0);
      }
      else
      {
        callback(1);
      }
*/    
}



function number_format( number, decimals, dec_point, thousands_sep ) {
    // http://kevin.vanzonneveld.net
    // +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +     bugfix by: Michael White (http://crestidg.com)
    // +     bugfix by: Benjamin Lupton
    // +     bugfix by: Allan Jensen (http://www.winternet.no)
    // +    revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)    
    // *     example 1: number_format(1234.5678, 2, '.', '');
    // *     returns 1: 1234.57     
 
    var n = number, c = isNaN(decimals = Math.abs(decimals)) ? 2 : decimals;
    var d = dec_point == undefined ? "," : dec_point;
    var t = thousands_sep == undefined ? "." : thousands_sep, s = n < 0 ? "-" : "";
    var i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}