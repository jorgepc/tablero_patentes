/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/* VARIABLES GLOBALES*/

var clave_indicador_actual = 1;
var clave_municipio_actual = '';
var localidad_actual = '';
var id_grafico_actual = -1;

var vista_actual = 'municipio';

var lista_id_valor_graficos_municipio = new Array();

var colores = [];
colores.push("0,0,255"); //Estado
colores.push("0,147,0"); //Municipio1
colores.push("255,128,64"); //Municipio2
colores.push("255,0,255"); //Municipio3
colores.push("0,255,0"); //Municipio4
colores.push("50,117,158"); //Municipio5
colores.push("0,255,255"); //Municipio6
colores.push("255,128,0"); //Municipio7
colores.push("128,0,255"); //Municipio8
colores.push("255,128,255"); //Municipio9
colores.push("255,255,0"); //Municipio10



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function actualiza_menu_indicadores()
{
	//$("#contenedor_municipio").load( "application/plantillas/vista_municipios.html");
	//$("#contenedor_localidad").load( "application/plantillas/vista_localidad.html");

	$("#menu_indicadores").empty();

      db.transaction(function (tx) {
            tx.executeSql("SELECT * FROM indicadores ORDER BY indicadore_id ASC", [], function (tx, results) 
            {
                  if (results.rows && results.rows.length) 
                  {
		                  for (i= 0; i < results.rows.length; i = i + 1) {
		                        $("#menu_indicadores").append('<li id="mid'+results.rows.item(i).indicadore_id+'" onclick="cambia_indicador_menu('+results.rows.item(i).indicadore_id+')" class="leftmenu-opt"><a class="more" href="#"> <div style="display:inline-block;line-height:30px;margin-top:10px;margin-bottom:10px;"> <table border=0 cellpadding=0 cellspacing=0><tr><td width=25><i class="icon-th icon-white"></i></td><td>'+results.rows.item(i).nombre+'</td></tr></table></div></a></li>');
		                  }
                  }
                  else
                  {
		                        $("#menu_indicadores").append('<li class="leftmenu-opt"><a class="more" href="#"><i class="icon-th icon-white"></i>No hay indicadores<span id="badge_lm1" class="leftmenu-badge hide"></span></a></li>');
                  }
            }, function (tx) {
                alert('Error al cargar la lista de notificaciones de la ruta');
            });
      });
}


function controlador_vista_tablero(indicadore_id)
{
	//return;
	clave_indicador_actual = indicadore_id;

	if(vista_actual == 'municipio')
		muestra_datos_municipio();
	else if(vista_actual == 'localidad')
		vista_datos_localidad()
}


function cambia_indicador_menu(indicadore_id)
{
		//Este codigo es solamente para deseleccionar el indicador seleccionado
		db.transaction(function (tx) {
		    tx.executeSql("SELECT * FROM indicadores", [], function (tx, results) 
		    {
		    	for (i= 0; i < results.rows.length; i = i + 1)
		    	{
		        	if(results.rows.item(i).indicadore_id == clave_indicador_actual)
		        	{
		              $("#txt_titulo_indicador").html(results.rows.item(i).nombre);
		              $("#txt_descripcion_indicador").html("Descripci&oacute;n: " + results.rows.item(i).descripcion);
		              $('#mid'+results.rows.item(i).indicadore_id).addClass('leftmenu-activeopt');            		
		        	}
		        	else
		        	{
		              $('#mid'+results.rows.item(i).indicadore_id).removeClass('leftmenu-activeopt');            		
		        	}
		    	}

		    }, function (tx) {
		        alert('Error en el select');
		    });
		});

		    	controlador_vista_tablero(indicadore_id);

}

function muestra_menu_otros_municipios()
{
	$("#myModal").modal('show');

}


function muestra_datos_localidad(id_grafico)
{
		id_grafico_actual = id_grafico;
		vista_actual = 'localidad';
		clave_municipio_actual = lista_id_valor_graficos_municipio[id_grafico];
		controlador_vista_tablero(clave_indicador_actual);
}


function fin_efecto_transicion()
{
	if(vista_actual == 'localidad')
		$('#mapa_localidades').highcharts().get(clave_municipio_actual).zoomTo();
}

function muestra_datos_municipio()
{	
      vista_actual = 'municipio';
      id_grafico_actual = -1;
      clave_municipio_actual = '';
//      $("#contenedor_vistas").load( "application/plantillas/vista_municipios.html", function() {

				//$("#tabla_municipios").empty();

				var datos_array = new Array();
				var datos_texto_array = new Array();
				var textos_color_array = new Array();
				lista_id_valor_graficos_municipio.length = 0;

				db.transaction(function (tx) {
				    tx.executeSql("SELECT MIN(elemento) as elemento FROM datos_municipio_indicadores WHERE indicadore_id = ?", [clave_indicador_actual], function (tx, results) 
				    {

						if(results.rows.item(0).elemento == 'unico')
						{
					          //$("#encabezado_tabla_datos").empty();
					          //$("#encabezado_tabla_datos").append('<tr><th colspan="2">El Estado y sus municipios</th></tr>');
					          $("#div_tabla_datos").html('<table class="table table-conddensed margin-0"><tbody id="tabla_municipios" style="font-size: 12px;"></tbody></table>');
						      
						      db.transaction(function (tx) {
						            tx.executeSql("SELECT * FROM datos_municipio_indicadores WHERE indicadore_id = ? ORDER BY porcentaje DESC", [clave_indicador_actual], function (tx, results) 
						            {
									            	var i;
									            	for (i= 0; i < results.rows.length; i = i + 1)
									            	{

											            		if(results.rows.item(i).municipio == 'QUINTANA ROO')
											            			$('#tabla_municipios').append('<tr><td><div style="width:4px;height:0;border:5px solid rgba('+colores[i]+', 0.65);overflow:hidden"></div></td><td>'+results.rows.item(i).municipio+'</td><td>'+number_format( results.rows.item(i).valor, 0, '.', ',' )+'</td></tr><tr><td></td><td></td><td></td></tr>');
											            		else
											            			$('#tabla_municipios').append('<tr><td><div style="width:4px;height:0;border:5px solid rgba('+colores[i]+', 0.65);overflow:hidden"></div></td><td>'+results.rows.item(i).municipio+'</td><td>'+number_format( results.rows.item(i).valor, 0, '.', ',' )+'</td></tr>');

											            		if(i >= 5)
											            		{            		
																	datos_array.push( [0, parseFloat(results.rows.item(i).porcentaje.toFixed(2))] );
																	textos_color_array.push( [results.rows.item(i).municipio, colores[i]] );
											            		}
											            		else
											            		{
												            		$("#chart-top"+i+"_vframe").html(results.rows.item(i).municipio);
																	datos_array.length = 0;
																	textos_color_array.length = 0;
																	datos_array[0] = [0, parseFloat(results.rows.item(i).porcentaje.toFixed(2))];
																	textos_color_array[0] = ["", colores[i]]; 
												            		//chart_animpie("chart-top"+(i+1), 0, results.rows.item(i).porcentaje.toFixed(), true, colores[i], "190,190,190");
												            		chart_animpie2("chart-top"+i, JSON.stringify(datos_array) , JSON.stringify(textos_color_array), 0 );

												            		lista_id_valor_graficos_municipio[i] = results.rows.item(i).clave_municipio; 

																	datos_array.length = 0;
																	textos_color_array.length = 0;
											            		}            			
									            	}
									            	//$("#chart-top5_vframe").html("Resto de los municipios");
									            	$("#chart-top5").width(400);
									            	chart_animpie2("chart-top5", JSON.stringify(datos_array) , JSON.stringify(textos_color_array), 4 );
									            	$('#cuadro_simbologia').html('<span style="font-size:15px; top: 1px">Resto de <br>los municipios</span>');
						
						            }, function (tx) {
						                alert('Error en el select');
						            });
								});
				    	}
						else
						{
					          //$("#encabezado_tabla_datos").empty();
					          //$("#encabezado_tabla_datos").append('<tr><th colspan="3">El Estado y sus municipios</th></tr>');
					          $("#div_tabla_datos").html('<table class="table table-conddensed margin-0"><tbody id="tabla_municipios" style="font-size: 12px;"></tbody></table>');
						      db.transaction(function (tx) {
						            tx.executeSql("SELECT datos_municipio_indicadores.*, x.total FROM datos_municipio_indicadores JOIN "+
						            			  "(SELECT indicadore_id, clave_municipio, SUM(valor) as total FROM datos_municipio_indicadores GROUP BY indicadore_id, clave_municipio) as x "+
						            			  "ON (datos_municipio_indicadores.indicadore_id = x.indicadore_id AND datos_municipio_indicadores.clave_municipio = x.clave_municipio) "+
						            			  "WHERE datos_municipio_indicadores.indicadore_id = ? ORDER BY x.total DESC, datos_municipio_indicadores.clave_municipio ASC, datos_municipio_indicadores.elemento ASC", [clave_indicador_actual], function (tx, results) 
						            {
						            		if(results.rows.length > 0)
						            		{
									            	var i;
									            	var j = 0;
									            	var i_colores = 0;
									            	var municipio_actual = results.rows.item(0).municipio;
									            	var elemento = '';
									            	var codigo = '';
									            	var codigo_total = '';
									            	var codigo_tabla_simbologia = '<div style="font-size:18px;color:#545454;margin-bottom:5px;">Simbolog&iacute;a</div> <table><tbody>';
									            	lista_id_valor_graficos_municipio.push(results.rows.item(0).clave_municipio);

									            	for (i= 0; i < results.rows.length; i = i + 1)
									            	{

														        if(results.rows.item(i).municipio == 'QUINTANA ROO')
														        {
														        	codigo_tabla_simbologia += '<tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px">'+
														        							   '<div style="width:8px;height:0;border:9px solid rgba('+colores[i_colores]+', 0.65);overflow:hidden"></div></div>'+
				                                                                               '</td><td class="legendLabel" style="font-size: 12px;">'+results.rows.item(i).elemento+'</td></tr>';
														        }

											            		if(j < 5)
											            		{
													            		if(municipio_actual != results.rows.item(i).municipio)
													            		{
														            		$("#chart-top"+j+"_vframe").html(municipio_actual);
														            		chart_animpie2("chart-top"+j, JSON.stringify(datos_array) , JSON.stringify(textos_color_array), 5 );
														            		codigo_total += '<tr><td valign="middle" rowspan="'+datos_array.length+'">'+codigo.substr(0, codigo.length - 4) ;
														            		codigo = '';

														            		codigo_total += '</tr><tr><td></td><td></td><td></td><td></td></tr>';

																			datos_array.length = 0;
																			textos_color_array.length = 0;
																			i_colores = 0;
																			lista_id_valor_graficos_municipio.push(results.rows.item(i).clave_municipio);

																			datos_array.push( [0, parseFloat(results.rows.item(i).porcentaje.toFixed(2))] );
																			textos_color_array.push( [results.rows.item(i).elemento, colores[i_colores]] );
																			municipio_actual = results.rows.item(i).municipio;
																			j++;
													            		}            			
													            		else
													            		{            		
																			datos_array.push( [0, parseFloat(results.rows.item(i).porcentaje.toFixed(2))] );
																			textos_color_array.push( [results.rows.item(i).elemento, colores[i_colores]] );
													            		}
											            		}
											            		else
											            		{

													            		if(municipio_actual != results.rows.item(i).municipio)
													            		{
														            		codigo_total += '<tr><td rowspan="'+datos_array.length+'">'+codigo.substr(0, codigo.length - 4) ;
														            		codigo = '';
														            		codigo_total += '</tr><tr><td></td><td></td><td></td></tr>';

																			datos_array.length = 0;
																			i_colores = 0;
																			datos_array.push( [0, parseFloat(results.rows.item(i).porcentaje.toFixed(2))] );
																			municipio_actual = results.rows.item(i).municipio;
													            		}            			
													            		else
													            		{            		
																			datos_array.push( [0, parseFloat(results.rows.item(i).porcentaje.toFixed(2))] );
													            		}

																	if(datos_texto_array[results.rows.item(i).elemento] == undefined)
																		datos_texto_array[results.rows.item(i).elemento] = parseFloat(results.rows.item(i).porcentaje.toFixed(2) );
																	else
																		datos_texto_array[results.rows.item(i).elemento] = parseFloat(datos_texto_array[results.rows.item(i).elemento]) + parseInt(results.rows.item(i).porcentaje.toFixed(2) );

																	j++;
																	//datos_texto_array.push([new Array('elemento',results.rows.item(i).elemento), new Array('valor', parseInt(results.rows.item(i).porcentaje.toFixed() )  ) ] );
											            		}

											            		if(elemento != results.rows.item(i).municipio)
											            		{
											            			codigo += results.rows.item(i).municipio+'</td>';
											            			elemento = results.rows.item(i).municipio;
											            		}
											            		
											            		codigo += '<td><div style="width:4px;height:0;border:5px solid rgba('+colores[i_colores]+', 0.65);overflow:hidden"></div></td>';
											            		codigo += '<td>'+results.rows.item(i).elemento+'</td>'+'<td>'+number_format( results.rows.item(i).valor, 0, '.', ',' )+'</td></tr>';
																codigo += '<tr>';

											            		i_colores++;

																if(i_colores > 10)
																	i_colores = 0;

									            	}

									            	codigo_total += '<tr><td rowspan="'+datos_array.length+'">'+codigo.substr(0, codigo.length - 4) ;
									            	$("#div_tabla_datos").html('<table class="table table-conddensed margin-0"><tbody id="tabla_municipios" style="font-size: 12px;">'+codigo_total+'</tbody></table>');
									            	//$('#tabla_municipios').html(codigo);

													datos_array.length = 0;
													textos_color_array.length = 0;
													i_colores = 0;
													var arr = [];

													for(property in datos_texto_array) {
														arr.push(property);
													    //console.log(property + " = " + datos_texto_array[property]);
													}

													for (var n=arr.length; n--; ){
													   var valor = datos_texto_array[arr[n]];

														datos_array.push( [0, parseFloat(valor.toFixed(2) )] );
														textos_color_array.push( [arr[n], colores[i_colores]] );
														i_colores++;
														if(i_colores > 10)
															i_colores = 0;
													}
													$("#chart-top5").width(250);
									            	chart_animpie2("chart-top5", JSON.stringify(datos_array) , JSON.stringify(textos_color_array), 5 );
									            	$('#cuadro_simbologia').html(codigo_tabla_simbologia+'</tbody></table>');
									            	//$('#cuadro_simbologia').html('<table style="position:absolute;top:5px;right:5px;;font-size:smaller;color:#545454"><thead><tr><th colspan="2" style="font-size:18px;color:#545454">Simbolog&iacute;a</th></tr></thead><tbody><tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid rgba(0,0,255, 0.65);overflow:hidden"></div></div></td><td class="legendLabel">10%</td></tr><tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid rgba(0,147,0, 0.65);overflow:hidden"></div></div></td><td class="legendLabel">3%</td></tr></tbody></table>');
											}

						            }, function (tx) {
						                alert('Error en el select');
						            });
								});
				    	}


				    }, function (tx) {
				        alert('Error en el select');
				    });
				});

      //}); //callback al cargar la vista

}



function vista_datos_localidad()
{
	//clave_municipio_actual = lista_id_valor_graficos_municipio[id_grafico];

	//$("#contenedor_vistas").load( "application/plantillas/vista_localidad.html", function() {

			var datos_array = new Array();
			var datos_texto_array = new Array();
			var textos_color_array = new Array();
			var datos_localidades_array = new Array();
			var datos_localidades2_array = new Array();
			lista_id_valor_graficos_municipio.length = 0;

			db.transaction(function (tx) {
			    tx.executeSql("SELECT MIN(elemento) as elemento FROM datos_localidad_indicadores WHERE indicadore_id = ? AND clave_municipio = ?", [clave_indicador_actual, clave_municipio_actual], function (tx, results) 
			    {

					if(results.rows.item(0).elemento == 'unico')
					{
				          $("#div_tabla_datos_localidad").html('<table class="table table-conddensed margin-0"><tbody id="tabla_localidades" style="font-size: 12px;"></tbody></table>');
					      
					      db.transaction(function (tx) {
					            tx.executeSql("SELECT * FROM datos_localidad_indicadores WHERE indicadore_id = ? AND clave_municipio = ? ORDER BY porcentaje DESC", [clave_indicador_actual, clave_municipio_actual], function (tx, results) 
					            {
								            	var i, j=0, resto_localidades = 0.0;
								            	for (i= 0; i < results.rows.length; i = i + 1)
								            	{

										            		$('#tabla_localidades').append('<tr><td><div style="width:4px;height:0;border:5px solid rgba('+colores[i]+', 0.65);overflow:hidden"></div></td><td>'+results.rows.item(i).localidad+'</td><td>'+number_format( results.rows.item(i).valor, 0, '.', ',' )+'</td></tr>');

										            		if(i >= 5)
										            		{            		
																resto_localidades += parseFloat(results.rows.item(i).porcentaje);
										            		}
										            		else
										            		{
											            		$("#chart-toploc"+j+"_vframe").html(results.rows.item(i).localidad);
																datos_array.length = 0;
																textos_color_array.length = 0;
																datos_array[0] = [0, parseFloat(results.rows.item(i).porcentaje.toFixed(2))];
																textos_color_array[0] = ["", colores[i]]; 
											            		//chart_animpie("chart-top"+(i+1), 0, results.rows.item(i).porcentaje.toFixed(), true, colores[i], "190,190,190");
											            		chart_animpie2("chart-toploc"+j, JSON.stringify(datos_array) , JSON.stringify(textos_color_array), 0 );

											            		//lista_id_valor_graficos_municipio[i] = results.rows.item(i).localidad; 
											            		j++;
																datos_array.length = 0;
																textos_color_array.length = 0;
										            		}

								            	}

								            	if( j < results.rows.length)
								            	{
													datos_array.length = 0;
													textos_color_array.length = 0;
									            	datos_array.push( [0, parseFloat(resto_localidades.toFixed(2) )] );
													textos_color_array.push( ['', colores[j]] );
									            	//$("#chart-toploc5").width(400);
									            	$("#chart-toploc4_vframe").html('Resto de las localidades');
									            	chart_animpie2("chart-toploc4", JSON.stringify(datos_array) , JSON.stringify(textos_color_array), 0 );
								            	}
								            	$('#cuadro_simbologia_localidad').html('');								            		
					
					            }, function (tx) {
					                alert('Error en el select');
					            });
							});
			    	}
					else
					{
				          $("#div_tabla_datos_localidad").html('<table class="table table-conddensed margin-0"><tbody id="tabla_localidades" style="font-size: 12px;"></tbody></table>');
					      db.transaction(function (tx) {
					            tx.executeSql("SELECT datos_localidad_indicadores.*, x.total FROM datos_localidad_indicadores JOIN "+
					            			  "(SELECT indicadore_id, clave_municipio, clave_localidad, SUM(valor) as total FROM datos_localidad_indicadores GROUP BY indicadore_id, clave_municipio, clave_localidad) as x "+
					            			  "ON (datos_localidad_indicadores.indicadore_id = x.indicadore_id AND datos_localidad_indicadores.clave_municipio = x.clave_municipio AND datos_localidad_indicadores.clave_localidad = x.clave_localidad) "+
					            			  "WHERE datos_localidad_indicadores.indicadore_id = ? AND datos_localidad_indicadores.clave_municipio = ? ORDER BY x.total DESC, datos_localidad_indicadores.clave_localidad ASC, datos_localidad_indicadores.elemento ASC", [clave_indicador_actual, clave_municipio_actual], function (tx, results) 
					            {
								            	var i;
								            	var j = 0;
								            	var i_colores = 0;
								            	var localidad_actual = results.rows.item(0).localidad;
								            	var elemento = '';
								            	var codigo = '';
								            	var codigo_total = '';
								            	var codigo_tabla_simbologia = '<div style="font-size:18px;color:#545454;margin-bottom:5px;">Simbolog&iacute;a</div> <table><tbody>';
								            	//lista_id_valor_graficos_municipio.push(results.rows.item(0).clave_localidad);

								            	for (i= 0; i < results.rows.length; i = i + 1)
								            	{

										            		if(j < 5)
										            		{
												            		if(localidad_actual != results.rows.item(i).localidad)
												            		{
													            		$("#chart-toploc"+j+"_vframe").html(localidad_actual);
													            		chart_animpie2("chart-toploc"+j, JSON.stringify(datos_array) , JSON.stringify(textos_color_array), 5 );
													            		codigo_total += '<tr><td valign="middle" rowspan="'+datos_array.length+'">'+codigo.substr(0, codigo.length - 4) ;
													            		codigo = '';

													            		codigo_total += '</tr><tr><td></td><td></td><td></td><td></td></tr>';

																		datos_array.length = 0;
																		textos_color_array.length = 0;
																		i_colores = 0;
																		//lista_id_valor_graficos_municipio.push(results.rows.item(i).clave_localidad);

																		datos_array.push( [0, parseFloat(results.rows.item(i).porcentaje.toFixed(2))] );
																		textos_color_array.push( [results.rows.item(i).elemento, colores[i_colores]] );
																		localidad_actual = results.rows.item(i).localidad;
																		j++;
												            		}            			
												            		else
												            		{            		
																		datos_array.push( [0, parseFloat(results.rows.item(i).porcentaje.toFixed(2))] );
																		textos_color_array.push( [results.rows.item(i).elemento, colores[i_colores]] );
												            		}
										            		}
										            		else
										            		{

												            		if(localidad_actual != results.rows.item(i).localidad)
												            		{
													            		codigo_total += '<tr><td rowspan="'+datos_array.length+'">'+codigo.substr(0, codigo.length - 4) ;
													            		codigo = '';
													            		codigo_total += '</tr><tr><td></td><td></td><td></td></tr>';

																		datos_array.length = 0;
																		i_colores = 0;
																		datos_array.push( [0, parseFloat(results.rows.item(i).porcentaje.toFixed(2))] );
																		localidad_actual = results.rows.item(i).localidad;
												            		}            			
												            		else
												            		{            		
																		datos_array.push( [0, parseFloat(results.rows.item(i).porcentaje.toFixed(2))] );
												            		}

																if(datos_texto_array[results.rows.item(i).elemento] == undefined)
																	datos_texto_array[results.rows.item(i).elemento] = parseFloat(results.rows.item(i).porcentaje.toFixed(2) );
																else
																	datos_texto_array[results.rows.item(i).elemento] = parseFloat(datos_texto_array[results.rows.item(i).elemento]) + parseFloat(results.rows.item(i).porcentaje.toFixed(2) );

																j++;
																//datos_texto_array.push([new Array('elemento',results.rows.item(i).elemento), new Array('valor', parseInt(results.rows.item(i).porcentaje.toFixed() )  ) ] );
										            		}

													        if(j == 0)
													        {
													        	codigo_tabla_simbologia += '<tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px">'+
													        							   '<div style="width:8px;height:0;border:9px solid rgba('+colores[i_colores]+', 0.65);overflow:hidden"></div></div>'+
			                                                                               '</td><td class="legendLabel" style="font-size: 12px;">'+results.rows.item(i).elemento+'</td></tr>';
													        }

										            		if(elemento != results.rows.item(i).localidad)
										            		{
										            			codigo += results.rows.item(i).localidad+'</td>';
										            			elemento = results.rows.item(i).localidad;
										            		}
										            		
										            		codigo += '<td><div style="width:4px;height:0;border:5px solid rgba('+colores[i_colores]+', 0.65);overflow:hidden"></div></td>';
										            		codigo += '<td>'+results.rows.item(i).elemento+'</td>'+'<td>'+number_format( results.rows.item(i).valor, 0, '.', ',' )+'</td></tr>';
															codigo += '<tr>';

										            		i_colores++;

															if(i_colores > 10)
																i_colores = 0;

								            	}
												
								            	codigo_total += '<tr><td rowspan="'+datos_array.length+'">'+codigo.substr(0, codigo.length - 4) ;
								            	$("#div_tabla_datos_localidad").html('<table class="table table-conddensed margin-0"><tbody id="tabla_localidades" style="font-size: 12px;">'+codigo_total+'</tbody></table>');

												$("#chart-toploc4_vframe").html(localidad_actual);
												if( j >= 5)
												{
														$("#chart-toploc4_vframe").html('Resto de las localidades');
														datos_array.length = 0;
														textos_color_array.length = 0;
														i_colores = 0;
														var arr = [];

														for(property in datos_texto_array) {
															arr.push(property);
														    //console.log(property + " = " + datos_texto_array[property]);
														}

														for (var n=arr.length; n--; ){
														   var valor = datos_texto_array[arr[n]];

															datos_array.push( [0, parseFloat(valor.toFixed(2) )] );
															textos_color_array.push( [arr[n], colores[i_colores]] );
															i_colores++;
															if(i_colores > 10)
																i_colores = 0;
														}
												}

												//$("#chart-toploc4").width(250);
								            	chart_animpie2("chart-toploc4", JSON.stringify(datos_array) , JSON.stringify(textos_color_array), 5 );
								            	$('#cuadro_simbologia_localidad').html(codigo_tabla_simbologia+'</tbody></table>');
								            	//$('#cuadro_simbologia').html('<table style="position:absolute;top:5px;right:5px;;font-size:smaller;color:#545454"><thead><tr><th colspan="2" style="font-size:18px;color:#545454">Simbolog&iacute;a</th></tr></thead><tbody><tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid rgba(0,0,255, 0.65);overflow:hidden"></div></div></td><td class="legendLabel">10%</td></tr><tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid rgba(0,147,0, 0.65);overflow:hidden"></div></div></td><td class="legendLabel">3%</td></tr></tbody></table>');
					

					            }, function (tx) {
					                alert('Error en el select');
					            });
							});
			    	}


			    }, function (tx) {
			        alert('Error en el select');
			    });
			});


			//Esto es para mostrar datos del mapa
	      db.transaction(function (tx) {
	            tx.executeSql("SELECT indicadore_id, clave_municipio, clave_localidad, SUM(porcentaje) as porcentaje FROM datos_localidad_indicadores WHERE indicadore_id = ? AND clave_municipio = ? GROUP BY indicadore_id, clave_municipio, clave_localidad ORDER BY porcentaje DESC", [clave_indicador_actual, clave_municipio_actual], function (tx, results) 
	            {
				            	var i;
				            	for (i= 0; i < results.rows.length; i = i + 1)
				            	{
						            	if(i < 4)
						            		datos_localidades_array.push('{"code": "23'+results.rows.item(i).clave_municipio+results.rows.item(i).clave_localidad+'","z": '+results.rows.item(i).porcentaje.toFixed(2)+'}');          			
						            	else
						            		datos_localidades2_array.push('{"code": "23'+results.rows.item(i).clave_municipio+results.rows.item(i).clave_localidad+'","z": '+results.rows.item(i).porcentaje.toFixed(2)+'}');          			
				            	}

								var texto_datos_localidades_array = "["+ datos_localidades_array.toString() + "]";
							    var data_localidades = JSON.parse(texto_datos_localidades_array);

								var texto_datos_localidades2_array = "["+ datos_localidades2_array.toString() + "]";
							    var data_localidades2 = JSON.parse(texto_datos_localidades2_array);

							    var data_municipios = JSON.parse('[{"id":"'+clave_municipio_actual+'","value":1}]');
							   /* var data_municipios = [
									{"id": "001","value": 1},
									{"id": "002","value": 1},
									{"id": "003","value": 1},
									{"id": "004","value": 1},
									{"id": "005","value": 1},
									{"id": "006","value": 1},
									{"id": "007","value": 1},
									{"id": "008","value": 1},
									{"id": "010","value": 1},
									{"id": "009","value": 1}];
*/
							    var municipios, localidades, localidades2;

								$.getJSON('municipios.json', function(geojson_municipios) {
								  
									    municipios  = Highcharts.geojson(geojson_municipios, 'map');

										$.getJSON('localidades.json', function(geojson_puntos) {

											    localidades = Highcharts.geojson(geojson_puntos, 'mappoint');
											    localidades2 = Highcharts.geojson(geojson_puntos, 'mappoint');

										        // Initiate the chart
										        $('#mapa_localidades').highcharts('Map', {

										            credits : {
										            	enabled: false
										            },

										            title : {
										                text : ''
										            },

										            legend: {
										                enabled: false
										            },

										            mapNavigation: {
										                enabled: true,
										                enableButtons: false,
										                enableDoubleClickZoom: true,
										                enableDoubleClickZoomTo: true,
										                enableMouseWheelZoom: false,
										                enableTouchZoom: false,
										                buttonOptions: {
										                    verticalAlign: 'bottom'
										                }
										            },

										            series : [{
										                name: 'Municipios',
										                data : data_municipios,
										                mapData : municipios,
										                joinBy: ['clave_municipio', 'id'],
										                color: Highcharts.getOptions().colors[2],
										                /*states: {
										                    hover: {
										                        color: Highcharts.getOptions().colors[4]
										                    }
										                },
										                dataLabels: {
										                    enabled: false,
										                    format: '{point.name}',
										                    style: {
										                        width: '80px' // force line-wrap
										                    }
										                },
										                */
										            }
										            ,{
											            name: 'Localidades por tamaño',
											            type: 'mapbubble',
											            data: data_localidades,
											            mapData: localidades,
											            allAreas: false,
											            joinBy: ['claveid', 'code'],
											            minSize: 6,
											            maxSize: '18%',
											            sizeBy: 'total',
										                dataLabels: {
										                	enabled: true,
										                    align: 'left',
										                    color: 'black',
										                    format: '{point.name}',
										                    verticalAlign: 'middle'
										                },
										                /*tooltip: {
										                	enabled: false,
										                    pointFormat: '{point.name}: {point.z}%'
										                }*/
											        },{
											            name: 'Localidades por tamaño2',
											            type: 'mapbubble',
											            data: data_localidades2,
											            mapData: localidades2,
											            allAreas: false,
											            joinBy: ['claveid', 'code'],
											            minSize: 6,
											            maxSize: '9%',
											            sizeBy: 'total',
										                /*tooltip: {
										                	enabled: false,
										                    pointFormat: '{point.name}: {point.z}'
										                }*/
											        }
										            /*,{
										                name: 'Localidades',
										                type: 'mappoint',
										                data: localidades,
										                color: 'black',
										                marker: {
										                    radius: 2
										                },
										                dataLabels: {
										                	enabled: false,
										                    align: 'left',
										                    verticalAlign: 'middle'
										                }/*,
										                animation: false,
										                tooltip: {
										                    pointFormat: '{point.name}'
										                }*/
										            //}
										            ]
										        });

												//$('#mapa_localidades').highcharts().get(clave_municipio_actual).zoomTo();


									    });
							    });

	
	            }, function (tx) {
	                alert('Error en el select');
	            });
			});



	//}); //callback al cargar la vista

}

