
	/*
		Main - common code
		Author: Thomas John (http://thomasjohn.eu), 2012
				
		Vars:
		
		log - should write messages? - console.log
		table_demodata1 - table demo
		table_demodata2 - table demo
		
		
		Functions: 
	
		colog(e) - log to the console
		topmenu_check(id_dropdown)
	*/
	
	
	//var
	
	var log = true;
	 
    var opt_not = false;
    var table_demodata1 = 0;
    var table_demodata2 = 0;

    
    $(window).resize(function() {

	});	
	
	function colog(e){
		if (log)
			console.log(e);	
	}
    
    function topmenu_check(id_dropdown){
    
    	return $('#'+id_dropdown).hasClass('open'); 
    }    
    
    function topmenu_register(id){
    
    	$('#'+id).on('mouseover', function () {
		
			if (topmenu_check("dropdown_"+id))
				return;
			
			$('#'+id).dropdown('toggle');
		
		})
    }
    
    
    //live
    
    function live_commonhandleData(txtres){
    	
    	var res=[];

    	//colog("Live - result: "+txtres);
    	
    	
    	//decode txtres
    	//...
    	
    	
    	//fake notifications
    			
    	switch (live_counter)
    	{
    		case 1:
    			
    			notification_add('Welcome to Final {Retina}.', 0, '2 hours',1,'application/data/users/me.jpg');
		
				notification_add('New message from the Community.', 5, '1 hour',2);
		
				notification_add('New e-mail from:', 1, '1 hour',8, 'application/data/users/c-bb2.jpg');

				notification_add('New user registration.', 2, '1 min',4);

    			notification_add('New order:', 3, '1 min',5, 'application/data/products/photo1.jpg');
    			
    			notification_add('Check Products.', 6, '1 min',6);
    			
    			notification_add('-', 6, '1 min',6);	/*example: wrong - same id*/
 		
    			notification_add('Check Calendar.', 7, '1 min',3);

    			notification_add('Add new product type.', 8, '1 min',7);
    			
    			break;
    	}
 		
    }   
    	
    	
    	
    //tables - handle all tables
    
	function table_getData(par){
	
		var id = par[0];

		//data request
		var req = "";
		
		//get options
		
		var sortcol, sortdesc, from, rowsn;
		
		sortcol = tables[id][4];
		sortdesc = tables[id][5];
		from = tables[id][10][1];
		rowsn = tables[id][10][2];
		
		//new values
		switch (par[1]){
		
			case 1:	//table update
				break;
			
			case 2:	//sort
				sortcol = par[2];
				sortdesc = par[3];
				break;
			
			case 3: //change row size
				rowsn = par[2];
				break;
				
			case 4:	//change index
				from = par[2];
				break;
		}

		//based on par and search form build req
		
		
		//server request
		
		//info
		$("#"+id).html("<div class=\"alert alert\">Loading data…</div>");
		
		if (id=="tab_1"){
		
			req = "application/data/orders.xml";
			par.push(req);
			
			//make request	
			live_request(req, table_setData, par);
		}
		else{	//static table
		
			setTimeout(function(){
			
				table_setData('', par);
			}, 500);
		}
		
	}
	
	function getValue(el){
	
    	if (el!=null && el[0].childNodes[0]!=null)
        	res = el[0].childNodes[0].nodeValue;
        else
        	res = "";
 
        
        return res;
	}
	
	function table_setData(txtres, par){
		
		var id, data, alldata, datasize;

			
		//parse data and table id
		
		id = par[0];
		
		
		//fake answer
		
		switch (id){
		
			case "tab_1":
			
				if (window.DOMParser){
				
  					var parser=new DOMParser();
  					var Data = parser.parseFromString(txtres, "text/xml");
  				}
				else {	// Internet Explorer
  					var Data=new ActiveXObject("Microsoft.XMLDOM");
  					Data.async=false;
  					Data.loadXML(txtres); 
  				} 
                
                
       			var Objects = Data.getElementsByTagName("order");

				data = [];
				for (var i=0; i<Objects.length; i++){
				
					var checked = getValue(Objects[i].getElementsByTagName("checked"));
					var oid = parseInt(getValue(Objects[i].getElementsByTagName("id")));
					var cart = parseInt(getValue(Objects[i].getElementsByTagName("cart")));
					var forname = getValue(Objects[i].getElementsByTagName("forname"));
					var surname = getValue(Objects[i].getElementsByTagName("surname"));
					var value = parseFloat(getValue(Objects[i].getElementsByTagName("value")));
					var status = getValue(Objects[i].getElementsByTagName("status"));
					
					data.push([checked, oid, cart, forname, surname, value, status]);
				}
				
				alldata = true;
				datasize = null;
				
				table_imback(id, par, data, alldata, datasize);
				
				break;
				
			case "tab_3":
			case "tab_4":
				if (table_demodata1==0)
				{
					//colog("No server access - demo data loaded.");
					table_demodata1++;
				}
				data = getFakeData();
				
				alldata = true;
				datasize = null;
				table_imback(id, par, data, alldata, datasize);
				
				break;
				
			case "tab_2":
				if (table_demodata2==1)
					//colog("No server access - demo data loaded.");
				
				table_demodata2++;
				data = getFakeData();
				
				alldata = false;
				datasize = 43;	//example
				table_imback(id, par, data, alldata, datasize);
				
				break;
		}
				
	}
	
	function table_open(id, row){
		
		alert("Open row: '"+id+"', "+row);
		
		//add code here
		
	}
	
	function table_action(id, type){
		
		alert("Action ("+type+") on table: "+id);
		
		//add code here
		
	}
	
	function label(data){
	
		switch (data)
		{
			case "OK":
			case "Active":
			case "Available":
				return "<span class=\"label label-success label-90\"><data></span>";
				break;
	
			case "Not Available":
				return "<span class=\"label label-important label-90\"><data></span>";
				break;
	
			default:
				return "<span class=\"label label-info label-90\"><data></span>";
		}
	}

	function owncompare(v1, v2, desc)	//example
	{
		//own compare example
	
		var comp=false;
				
		//compare here
		comp=v2<v1; //standard
		
		//common
		if (desc)
			return !comp;
		return comp;
	}
	
	